package algorithmsImplementation;

import java.sql.*;  
import java.util.*;
import java.lang.Long;


/*
 * Written by Shah Muhammad Hamdi
 * Date: 12/29/2015
 */
public class JFilter {
	
	public static double getJaccard(Long[] insid, Array ins_arr, String tablename)
	{
		Constants.callToJ++;
		double J = 0.0;
		Connection con = null;
		String sql;
		try{
			con = DBConnection.getConnection();
			
			
			Long deltaT = Constants.getDeltaT();
			
			//Getting intersection area
			sql = "SELECT ST_AsText(intersection_geom) FROM \""+tablename+"\" WHERE ins_arr = '"+ins_arr+"';";
			Statement stmtI2 = con.createStatement();
			ResultSet rsI2 = stmtI2.executeQuery(sql);
			double intersectionArea = 0.0;
			while(rsI2.next())
			{
				String currentMultiPolygon = rsI2.getString(1);
				intersectionArea += SpatialUtilities.getArea(currentMultiPolygon);
			}
			stmtI2.close();
			rsI2.close();
			
			//Getting intersection volume
			double intersectionVolume = intersectionArea * (double)deltaT;
						
			//Getting Union Time
			Long[] minTime = new Long[insid.length];
			Long[] maxTime = new Long[insid.length];
			//Getting timelength of occurance of all trajectories
			for(int i=0;i<tablename.length();i++)
			{
				ResultSet rsU1, rsU2;
				Statement stmtU1, stmtU2;
				sql = "SELECT MIN(generation_time) FROM \""+tablename.charAt(i)+"\" WHERE instance_id = "+insid[i]+";";//YYYYYYYYY
				stmtU1 = con.createStatement();
				rsU1 = stmtU1.executeQuery(sql);
				rsU1.next();
				minTime[i] =rsU1.getLong(1);
				stmtU1.close();
				rsU1.close();
				
				sql = "SELECT MAX(generation_time) FROM \""+tablename.charAt(i)+"\" WHERE instance_id = "+insid[i]+";";//YYYYYYYYY
				stmtU2 = con.createStatement();
				rsU2 = stmtU2.executeQuery(sql);
				rsU2.next();
				maxTime[i] =rsU2.getLong(1);
				stmtU2.close();
				rsU2.close();
			}
			
			long ultMin = Utilities.getMinLong(minTime);
			long ultMax = Utilities.getMaxLong(maxTime);
			
			//Getting Union area
			
			double unionArea = 0.0;
			//Going to each valid timestamp, collecting geometries of instances of different types and getting their union
			//and summing them to get area; finally multiplying with time to get volume
			for(long i=ultMin; i<=ultMax; i += deltaT) 
			{
				String[] collectedPolygons = new String[insid.length];
				for(int j=0;j<tablename.length();j++)
				{
					ResultSet rsU3;
					Statement stmtU3;
					stmtU3 = con.createStatement();
					sql = "SELECT ST_AsText(polygon_coordinates) FROM \""+tablename.charAt(j)+"\" WHERE instance_id = "+insid[j]+""
						   + " AND generation_time = "+i+";"; 
					rsU3 = stmtU3.executeQuery(sql);
					if(!rsU3.next())
					{
						collectedPolygons[j] = "GEOMETRYCOLLECTION EMPTY";
					}
					else
						collectedPolygons[j] = rsU3.getString(1);
					stmtU3.close();
					rsU3.close();
				}
				unionArea = unionArea + SpatialUtilities.getUnionArea(collectedPolygons);
			}
			double unionVolume = unionArea * (double) deltaT;
			
		    J = intersectionVolume/unionVolume;
		    
		}catch(Exception e){
		System.out.println("Error in getting Jaccard of two trajectories");
		System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		return 0.0;
		}
		return J;
	}
	public static boolean inTrash(ArrayList<Long> patIns)
	{
		for(ArrayList<Long> x: PatternAnalyzer.JaccardTrash)
			if(patIns.containsAll(x)){
				return true;
			}
		return false;
	}
	public static void deleteJaccardFailedRows(List<String> c, int k)
	{
		double J;
		Connection con;
	    Statement stmt;
	    String sql;
	    
	    try{
		con = DBConnection.getConnection();
		stmt =con.createStatement();
		
	    //Get J value for each tuple of each table instance
	    //Delete J failed rows

	    for(int i=0;i<c.size();i++)
		{
	    	String tablename = c.get(i);
	    	Formatter output1 = new Formatter(tablename+"_all_CCE_Test.txt");
	    	Formatter output2 = new Formatter(tablename+"_passed_CCE_Test.txt");
		    Statement stmt1 = null ;
			sql = "SELECT DISTINCT ins_arr FROM \""+tablename+"\";";
			stmt1 =con.createStatement();
			ResultSet rs = stmt1.executeQuery(sql) ;
			while(rs.next())
			{
				Array ins_arr = rs.getArray("ins_arr");
				Long[] insid = (Long[])ins_arr.getArray(); 
				ArrayList<Long> patternInstanceAL = new ArrayList<Long>(Arrays.asList(insid));
				
				if(!inTrash(patternInstanceAL))
				{
					J = JFilter.getJaccard(insid, ins_arr, tablename);
					output1.format("%s --> %f\n", Arrays.toString(insid), J);
					if(J < Constants.CCE_TH)
					{
						PatternAnalyzer.JaccardTrash.add(patternInstanceAL);
						sql = "DELETE FROM \""+tablename+"\" WHERE ins_arr = '"+ins_arr+"';";
						stmt.executeUpdate(sql);
					}
					else
						output2.format("%s --> %f\n", Arrays.toString(insid), J);
				}
				else
				{
					PatternAnalyzer.JaccardTrash.add(patternInstanceAL);
					sql = "DELETE FROM \""+tablename+"\" WHERE ins_arr = '"+ins_arr+"';";
					stmt.executeUpdate(sql);
				}
			}
			output1.close();
			output2.close();
			stmt1.close();
			rs.close();
		}
		stmt.close();
		
	    } catch(Exception e){
	    	System.out.println("Error in select or update query by J");
		    System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		    System.exit(0);
		}
	 }
	

}
