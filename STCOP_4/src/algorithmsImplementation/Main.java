package algorithmsImplementation;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		double[] PI_base = {0.2, 0.15, 0.1, 0.05};
		double[] CCE_base = {0.1, 0.0316227766, 0.01, 0.00316227766};
		
		Formatter output_3_months, output_6_months, output_12_months, output_test_arti_4_features, output_real_test_6_features; 

		try {
			
			//Test real and test arti
			/*
			output_test_arti_4_features = new Formatter("Performance compare on test artificial dataset 4 features.txt");
			Constants.FILE_PATH = "F:\\STCOP_data_directory\\test arti\\"; //change it according to file system
			Constants.dbName = "test_artificial_4_features";
			output_test_arti_4_features.format("%s\n\n", "Test articial 4 fetures");
			System.out.printf("%s\n\n", "Test articial 4 fetures");
			
			DBConnection.establishConnnectionNoGUI();	
			WKTReaderMaker.initializeWKTReader();
			Constants.initializeConsantsParameters();
			Utilities.setSpatialIntersectionExceptionHandler();
			if (! Constants.dbExists) 
			{
				long loadStart = System.currentTimeMillis();
				LoadDataInTablesFromFiles.initializeData();
				List<String> possiblePatterns = new ArrayList<String>();
				possiblePatterns = SetTransactions.generateTableInstances();
				SetTransactions.unionAllTableInstances(possiblePatterns);
				SetTransactions.deleteDuplicateTransactions();
				long loadEnd = System.currentTimeMillis();
				long loadTime = loadEnd - loadStart;
				System.out.println("Data load time (with transactionalizing) from "+ Constants.FILE_PATH + " is "+ (double)loadTime/1000 +" seconds" );
			}
			
			System.out.println(Constants.dbName+ " exists with st_transactions and a to f tables... starting programs");
			Constants.setNumberOfInstances();
			//call functions here
			//DBConnection.closeConnection();
			for(int i=0;i<PI_base.length;i++)
				for(int j=0;j<CCE_base.length;j++)
				{
					Constants.PI_TH = PI_base[i];
					Constants.CCE_TH = CCE_base[j];
					output_test_arti_4_features.format("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					System.out.printf("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					output_test_arti_4_features.format("%s\n", "FastSTCOP_Apriori");
					System.out.printf("%s\n", "FastSTCOP_Apriori");
					long programStartApriori = System.currentTimeMillis();
					FastSTCOP_Apriori.FastSTCOPCaller();
					long programEndApriori = System.currentTimeMillis();
					long programTimeApriori = programEndApriori - programStartApriori;
					output_test_arti_4_features.format("Runtime: %f seconds\n", (double)programTimeApriori/1000);
					System.out.printf("Runtime: %f seconds\n", (double)programTimeApriori/1000);
					output_test_arti_4_features.format("Number of CCE calculations: %d\n", Constants.callToJ);
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					output_test_arti_4_features.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					
					output_test_arti_4_features.format("%s\n", "Pattern Growth Approach");
					System.out.printf("%s\n", "Pattern Growth Approach");
					long programStartPG = System.currentTimeMillis();
					PatternGrowthSTCOP.PGCaller();
					long programEndPG = System.currentTimeMillis();
					long programTimePG = programEndPG - programStartPG;
					output_test_arti_4_features.format("Runtime: %f seconds\n", (double)programTimePG/1000);
					System.out.printf("Runtime: %f seconds\n", (double)programTimePG/1000);
					output_test_arti_4_features.format("Number of CCE calculations: %d\n", Constants.callToJ);
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					output_test_arti_4_features.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
				}
			DBConnection.closeConnection();
			output_test_arti_4_features.close();
			
			
			
			
			output_real_test_6_features = new Formatter("Performance compare on test real dataset 6 features.txt");
			Constants.FILE_PATH = "F:\\STCOP_data_directory\\test features\\"; //change it according to file system
			Constants.dbName = "test_real_6_features";
			output_real_test_6_features.format("%s\n\n", "Test real 6 fetures");
			System.out.printf("%s\n\n", "Test real 6 fetures");
			
			DBConnection.establishConnnectionNoGUI();	
			WKTReaderMaker.initializeWKTReader();
			Constants.initializeConsantsParameters();
			Utilities.setSpatialIntersectionExceptionHandler();
			if (! Constants.dbExists) 
			{
				long loadStart = System.currentTimeMillis();
				LoadDataInTablesFromFiles.initializeData();
				List<String> possiblePatterns = new ArrayList<String>();
				possiblePatterns = SetTransactions.generateTableInstances();
				SetTransactions.unionAllTableInstances(possiblePatterns);
				SetTransactions.deleteDuplicateTransactions();
				long loadEnd = System.currentTimeMillis();
				long loadTime = loadEnd - loadStart;
				System.out.println("Data load time (with transactionalizing) from "+ Constants.FILE_PATH + " is "+ (double)loadTime/1000 +" seconds" );
			}
			
			System.out.println(Constants.dbName+ " exists with st_transactions and a to f tables... starting programs");
			Constants.setNumberOfInstances();
			//call functions here
			//DBConnection.closeConnection();
			for(int i=0;i<PI_base.length;i++)
				for(int j=0;j<CCE_base.length;j++)
				{
					Constants.PI_TH = PI_base[i];
					Constants.CCE_TH = CCE_base[j];
					output_real_test_6_features.format("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					System.out.printf("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					output_real_test_6_features.format("%s\n", "FastSTCOP_Apriori");
					System.out.printf("%s\n", "FastSTCOP_Apriori");
					long programStartApriori = System.currentTimeMillis();
					FastSTCOP_Apriori.FastSTCOPCaller();
					long programEndApriori = System.currentTimeMillis();
					long programTimeApriori = programEndApriori - programStartApriori;
					output_real_test_6_features.format("Runtime: %f seconds\n", (double)programTimeApriori/1000);
					System.out.printf("Runtime: %f seconds\n", (double)programTimeApriori/1000);
					output_real_test_6_features.format("Number of CCE calculations: %d\n", Constants.callToJ);
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					output_real_test_6_features.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					
					output_real_test_6_features.format("%s\n", "Pattern Growth Approach");
					System.out.printf("%s\n", "Pattern Growth Approach");
					long programStartPG = System.currentTimeMillis();
					PatternGrowthSTCOP.PGCaller();
					long programEndPG = System.currentTimeMillis();
					long programTimePG = programEndPG - programStartPG;
					output_real_test_6_features.format("Runtime: %f seconds\n", (double)programTimePG/1000);
					System.out.printf("Runtime: %f seconds\n", (double)programTimePG/1000);
					output_real_test_6_features.format("Number of CCE calculations: %d\n", Constants.callToJ);
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					output_real_test_6_features.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
				}
			DBConnection.closeConnection();
			output_real_test_6_features.close();
			
			*/
			/////////////////////////////////////////////////////////////////////////////
			
			//Real 3 months, 6 months and 12 months
			output_3_months = new Formatter("Performance compare on 3 months data.txt");
			Constants.FILE_PATH = "F:\\STCOP_data_directory\\data\\3Mo\\"; //change it according to file system
			Constants.dbName = "real_quarter";
			output_3_months.format("%s\n\n", "3 Months data");
			System.out.printf("%s\n\n", "3 Months data");
			
			DBConnection.establishConnnectionNoGUI();	
			WKTReaderMaker.initializeWKTReader();
			Constants.initializeConsantsParameters();
			Utilities.setSpatialIntersectionExceptionHandler();
			if (! Constants.dbExists) 
			{
				long loadStart = System.currentTimeMillis();
				LoadDataInTablesFromFiles.initializeData();
				List<String> possiblePatterns = new ArrayList<String>();
				possiblePatterns = SetTransactions.generateTableInstances();
				SetTransactions.unionAllTableInstances(possiblePatterns);
				SetTransactions.deleteDuplicateTransactions();
				long loadEnd = System.currentTimeMillis();
				long loadTime = loadEnd - loadStart;
				System.out.println("Data load time (with transactionalizing) from "+ Constants.FILE_PATH + " is "+ (double)loadTime/1000 +" seconds" );
			}
			
			System.out.println(Constants.dbName+ " exists with st_transactions and a to f tables... starting programs");
			Constants.setNumberOfInstances();
			//call functions here
			//DBConnection.closeConnection();
			for(int i=0;i<PI_base.length;i++)
				for(int j=0;j<CCE_base.length;j++)
				{
					Constants.PI_TH = PI_base[i];
					Constants.CCE_TH = CCE_base[j];
					output_3_months.format("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					System.out.printf("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					output_3_months.format("%s\n", "FastSTCOP_Apriori");
					System.out.printf("%s\n", "FastSTCOP_Apriori");
					long programStartAP = System.currentTimeMillis();
					FastSTCOP_Apriori.FastSTCOPCaller();
					long programEndAP = System.currentTimeMillis();
					long programTimeAP = programEndAP - programStartAP;
					output_3_months.format("Runtime: %f seconds\n", (double)programTimeAP/1000);
					System.out.printf("Runtime: %f seconds\n", (double)programTimeAP/1000);
					output_3_months.format("Number of CCE calculations: %d\n", Constants.callToJ);
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					output_3_months.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					
					output_3_months.format("%s\n", "Pattern Growth Approach");
					System.out.printf("%s\n", "Pattern Growth Approach");
					long programStartPG = System.currentTimeMillis();
					PatternGrowthSTCOP.PGCaller();
					long programEndPG = System.currentTimeMillis();
					long programTimePG = programEndPG - programStartPG;
					output_3_months.format("Runtime: %f seconds\n", (double)programTimePG/1000);
					System.out.printf("Runtime: %f seconds\n", (double)programTimePG/1000);
					output_3_months.format("Number of CCE calculations: %d\n", Constants.callToJ);
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					output_3_months.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
				}
			DBConnection.closeConnection();
			output_3_months.close();
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			output_6_months = new Formatter("Performance compare on 6 months data.txt");
			Constants.FILE_PATH = "F:\\STCOP_data_directory\\data\\6Mo\\"; //change it according to file system
			Constants.dbName = "real_half";
			output_6_months.format("%s\n\n", "6 Months data");
			System.out.printf("%s\n\n", "6 Months data");
			
			DBConnection.establishConnnectionNoGUI();	
			WKTReaderMaker.initializeWKTReader();
			Constants.initializeConsantsParameters();
			Utilities.setSpatialIntersectionExceptionHandler();
			if (! Constants.dbExists) 
			{
				long loadStart = System.currentTimeMillis();
				LoadDataInTablesFromFiles.initializeData();
				List<String> possiblePatterns = new ArrayList<String>();
				possiblePatterns = SetTransactions.generateTableInstances();
				SetTransactions.unionAllTableInstances(possiblePatterns);
				SetTransactions.deleteDuplicateTransactions();
				long loadEnd = System.currentTimeMillis();
				long loadTime = loadEnd - loadStart;
				System.out.println("Data load time (with transactionalizing) from "+ Constants.FILE_PATH + " is "+ (double)loadTime/1000 +" seconds" );
			}
			System.out.println(Constants.dbName+ " exists with st_transactions and a to f tables... starting programs");
			Constants.setNumberOfInstances();
			//call functions here
			//DBConnection.closeConnection();
			for(int i=0;i<PI_base.length;i++)
				for(int j=0;j<CCE_base.length;j++)
				{
					Constants.PI_TH = PI_base[i];
					Constants.CCE_TH = CCE_base[j];
					output_6_months.format("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					System.out.printf("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					output_6_months.format("%s\n", "FastSTCOP_Apriori");
					System.out.printf("%s\n", "FastSTCOP_Apriori");
					long programStartAP = System.currentTimeMillis();
					FastSTCOP_Apriori.FastSTCOPCaller();
					long programEndAP = System.currentTimeMillis();
					long programTimeAP = programEndAP - programStartAP;
					output_6_months.format("Runtime: %f seconds\n", (double)programTimeAP/1000);
					System.out.printf("Runtime: %f seconds\n", (double)programTimeAP/1000);
					output_6_months.format("Number of CCE calculations: %d\n", Constants.callToJ);
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					output_6_months.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					
					output_6_months.format("%s\n", "Pattern Growth Approach");
					System.out.printf("%s\n", "Pattern Growth Approach");
					long programStartPG = System.currentTimeMillis();
					PatternGrowthSTCOP.PGCaller();
					long programEndPG = System.currentTimeMillis();
					long programTimePG = programEndPG - programStartPG;
					output_6_months.format("Runtime: %f seconds\n", (double)programTimePG/1000);
					System.out.printf("Runtime: %f seconds\n", (double)programTimePG/1000);
					output_6_months.format("Number of CCE calculations: %d\n", Constants.callToJ);
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					output_6_months.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
				}
			DBConnection.closeConnection();
			output_6_months.close();
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			output_12_months = new Formatter("Performance compare on 12 months data.txt");
			Constants.FILE_PATH = "F:\\STCOP_data_directory\\data\\12Mo\\"; //change it according to file system
			Constants.dbName = "real_year";
			output_12_months.format("%s\n\n", "12 Months data");
			System.out.printf("%s\n\n", "12 Months data");
			
			DBConnection.establishConnnectionNoGUI();	
			WKTReaderMaker.initializeWKTReader();
			Constants.initializeConsantsParameters();
			Utilities.setSpatialIntersectionExceptionHandler();
			if (! Constants.dbExists) 
			{
				long loadStart = System.currentTimeMillis();
				LoadDataInTablesFromFiles.initializeData();
				List<String> possiblePatterns = new ArrayList<String>();
				possiblePatterns = SetTransactions.generateTableInstances();
				SetTransactions.unionAllTableInstances(possiblePatterns);
				SetTransactions.deleteDuplicateTransactions();
				long loadEnd = System.currentTimeMillis();
				long loadTime = loadEnd - loadStart;
				System.out.println("Data load time (with transactionalizing) from "+ Constants.FILE_PATH + " is "+ (double)loadTime/1000 +" seconds" );
			}
			System.out.println(Constants.dbName+ " exists with st_transactions and a to f tables... starting programs");
			Constants.setNumberOfInstances();
			//call functions here
			//DBConnection.closeConnection();
			for(int i=0;i<PI_base.length;i++)
				for(int j=0;j<CCE_base.length;j++)
				{
					Constants.PI_TH = PI_base[i];
					Constants.CCE_TH = CCE_base[j];
					output_12_months.format("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					System.out.printf("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					output_12_months.format("%s\n", "FastSTCOP_Apriori");
					System.out.printf("%s\n", "FastSTCOP_Apriori");
					long programStartAP = System.currentTimeMillis();
					FastSTCOP_Apriori.FastSTCOPCaller();
					long programEndAP = System.currentTimeMillis();
					long programTimeAP = programEndAP - programStartAP;
					output_12_months.format("Runtime: %f seconds\n", (double)programTimeAP/1000);
					System.out.printf("Runtime: %f seconds\n", (double)programTimeAP/1000);
					output_12_months.format("Number of CCE calculations: %d\n", Constants.callToJ);
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					output_12_months.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					
					output_12_months.format("%s\n", "Pattern Growth Approach");
					System.out.printf("%s\n", "Pattern Growth Approach");
					long programStartPG = System.currentTimeMillis();
					PatternGrowthSTCOP.PGCaller();
					long programEndPG = System.currentTimeMillis();
					long programTimePG = programEndPG - programStartPG;
					output_12_months.format("Runtime: %f seconds\n", (double)programTimePG/1000);
					System.out.printf("Runtime: %f seconds\n", (double)programTimePG/1000);
					output_12_months.format("Number of CCE calculations: %d\n", Constants.callToJ);
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					output_12_months.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
				}
			DBConnection.closeConnection();
			output_12_months.close();
			
			
			
			
		} catch (Exception e) {
			System.out.println("Formatter problem");
			e.printStackTrace();
		}		
		
	}

}
