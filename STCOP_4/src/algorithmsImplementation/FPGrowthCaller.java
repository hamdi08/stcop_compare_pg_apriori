package algorithmsImplementation;

import java.io.File;


import GeneralFPGrowth.*;
import java.util.*;

public class FPGrowthCaller {
	
	static int threshold = 1;
	static String file = "/home/hamdi/PG_files/transaction";
	static Map<String, Integer> protoPatterns;
	static Map<String, Integer> cleanedProtoPatterns = new HashMap<String, Integer>();
	
	public static void FPGrowthonTransactionFile()
	{
		System.out.println("FP Growth for finding proto patterns begins...");
		try{
        FPGrowth fp = new FPGrowth(new File(file), threshold);
        protoPatterns = fp.getProtoPatterns();
		} catch(Exception e){
			System.out.println("Can't pass file to FP Growth ");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			System.exit(0);
		}
	}
	public static void cleanProtoPatterns()
	{
		for(String f:protoPatterns.keySet())
		{
			Integer support = protoPatterns.get(f);
			char[] charFeature = f.replaceAll(" ", "").toCharArray();
			if(charFeature.length <= 1)
				continue;
			Arrays.sort(charFeature);
			String strFeature = new String(charFeature);
			cleanedProtoPatterns.put(strFeature, support);
		}
		
	}

}
