package algorithmsImplementation;

//import java.sql.*;  
import java.util.*;

/*
 * Written by: Shah Muhammad Hamdi
 * Date: 12/23/2015
 */

public class FastSTCOP_Apriori {
	
	public static void FastSTCOPCaller()
	{
		System.out.println("FastSTCOP Apriori begins for "+Constants.FILE_PATH);
		int k = 1;
		List<String> c = new ArrayList<String>();
		List<String> final_pattern_base = new ArrayList<String>();
		Constants.callToJ = 0;
		Constants.numberOfPatternsForCCETest = 0;
		//Constants.setNumberOfInstances();
		int size1Candidate = 97; // Initial candidates are 'a', 'b', 'c' and so on. 
		
	    for(int i=0;i<Constants.numberOfEventTypes;i++)
	    {
	      c.add(i, Character.toString(((char)(size1Candidate + i))));
	    }
	    while(c.size() > 0)
		{
			c = Utilities.selfjoin(c, c, final_pattern_base, k);
			List<String> ctemp = new ArrayList<String>();
		    ctemp.addAll(c);
			for (String candidate : c) 
			{
				Constants.numberOfPatternsForCCETest ++;
				double PI = PatternAnalyzer.getParticipationIndexofPatternsForApriori(candidate);
				if(PI < Constants.PI_TH)
					ctemp.remove(candidate);
				
			}
			c.clear();
			c.addAll(ctemp);
			ctemp.clear();
			final_pattern_base.addAll(c);
			k = k + 1;
		}
	    Utilities.clearGlobalVariables();
	    System.out.println("No. of patterns: "+ final_pattern_base.size());
		Utilities.showOrginalPatterns(final_pattern_base);
		System.out.println("FastSTCOP Apriori ends for "+Constants.FILE_PATH);
	}
	

	public static void main(String[] args) {
		
		int k = 1;
		List<String> c = new ArrayList<String>();
		List<String> final_pattern_base = new ArrayList<String>();
		
		try
	    {
	    //Get connection with the database
		System.out.println("FastSTCOP Apriori begins...");
		DBConnection.establishConnnection();
		Constants.callToJ = 0;
		Constants.numberOfPatternsForCCETest = 0;
		long startTime = System.currentTimeMillis();
	    
	    //Initializing WKTReader object
	    WKTReaderMaker.initializeWKTReader();
	    Constants.initializeConsantsParameters();
	    
		//Load tables with data
	    long loadStart = System.currentTimeMillis();
		//LoadDataInTablesFromFiles.initializeData();
		long loadEnd = System.currentTimeMillis();
		long loadTime = loadEnd - loadStart;
		Constants.setNumberOfInstances();
		System.out.println("Data load time: "+(double)loadTime/1000+" seconds");
		
		//Initializing current pattern base
		int size1Candidate = 97; // Initial candidates are 'a', 'b', 'c' and so on. 
		
	    for(int i=0;i<Constants.numberOfEventTypes;i++)
	    {
	      c.add(i, Character.toString(((char)(size1Candidate + i))));
	    }
		
			
		while(c.size() > 0)
		{
			c = Utilities.selfjoin(c, c, final_pattern_base, k);
			List<String> ctemp = new ArrayList<String>();
		    ctemp.addAll(c);
			for (String candidate : c) 
			{
				System.out.println("Calculating PI with CCE for size "+(int)(k+1)+" candidate: "+ candidate);
				Constants.numberOfPatternsForCCETest ++;
				double PI = PatternAnalyzer.getParticipationIndexofPatternsForApriori(candidate);
				System.out.println("PI of "+ candidate+ " : "+PI);
				if(PI < Constants.PI_TH)
					ctemp.remove(candidate);
				
			}
			c.clear();
			c.addAll(ctemp);
			ctemp.clear();
			final_pattern_base.addAll(c);
			k = k + 1;
		}
		
		System.out.println("Final STCOPs...");
		System.out.println(final_pattern_base);
		System.out.println("No. of patterns: "+ final_pattern_base.size());
		Utilities.showOrginalPatterns(final_pattern_base);
		DBConnection.closeConnection();
        long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		long actualRuntime = totalTime - loadTime;
		System.out.println("Run time: " + (double)totalTime/1000+ " seconds");
		System.out.println("Load time: "+ (double)loadTime/1000+ " sec");
		System.out.println("Actual run time: "+ (double)actualRuntime/1000 + " sec");
		System.out.println("Calls to Jaccard: "+Constants.callToJ);
	    } //try 1
	    catch (Exception e){
	    System.out.println("Error in DB connection in FastSTCOP_Ariori");
	    System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	    System.exit(0);
	    }		
		System.out.println("FastSTCOP Apriori Terminated successfully"); 
	}

}
