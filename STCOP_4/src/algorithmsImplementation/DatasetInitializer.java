package algorithmsImplementation;

import java.util.ArrayList;
import java.util.List;

public class DatasetInitializer {
	public static void main(String[] args){
		try{
			DBConnection.establishConnnection();
			long startTime = System.currentTimeMillis();
			WKTReaderMaker.initializeWKTReader();
			Constants.initializeConsantsParameters();
			Utilities.setSpatialIntersectionExceptionHandler();
			LoadDataInTablesFromFiles.initializeData();
			List<String> possiblePatterns = new ArrayList<String>();
			possiblePatterns = SetTransactions.generateTableInstances();
			SetTransactions.unionAllTableInstances(possiblePatterns);
			SetTransactions.deleteDuplicateTransactions();
			SetTransactions.makeTransactionFile();
			long endTime = System.currentTimeMillis();	
			long totalTime = endTime - startTime;
			DBConnection.closeConnection();
			System.out.println("Data load time from "+ Constants.FILE_PATH + " is "+ (double)totalTime/1000 +" seconds" );
		}catch(Exception e){
			System.out.println("Error in initializing dataset");
			e.printStackTrace();
			System.exit(0);
		}
	}
	


}
