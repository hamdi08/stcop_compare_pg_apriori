package algorithmsImplementation;

import java.sql.*;  
import java.util.*;

/*
 * Written by: Shah Muhammad Hamdi
 * Date: 12/26/2015
 */
public class PIRefiner {
	public static List<String> deletePIFailedPatterns(List<String> c, int k)
	{
		double[] pRatio = new double[c.get(0).length()];
		double pIndex;
		Connection con = null ;
	    String sql;
	    List<String> ctemp = new ArrayList<String>();
	    ctemp.addAll(c);
	    try{
		con = DBConnection.getConnection();
		for(int i=0;i<c.size();i++)
		{
			String tablename = c.get(i);
			for(int j=0;j<tablename.length();j++)
			{
				Statement stmt1;
				ResultSet rs1;
				sql = "SELECT COUNT (DISTINCT ins_arr["+(int)(j+1)+"]) FROM \""+tablename+"\";";
				stmt1 = con.createStatement();
				rs1 = stmt1.executeQuery(sql);
				rs1.next();
				int x = rs1.getInt(1);
				stmt1.close();
				rs1.close();
				
				
				int index =(int)(tablename.charAt(j)-97);
				int y = Constants.no_of_instances[index];
				
				pRatio[j] = (double)x/(double)y;
			}
			pIndex = Utilities.getMin(pRatio);
			//pi debug
			System.out.println("Participation Index of "+tablename+":"+pIndex);
			if(pIndex < Constants.PI_TH)
			{
				Statement stmt = null;
				stmt = con.createStatement();
				sql = "DROP TABLE \""+tablename+"\";";
				stmt.execute(sql);
				ctemp.remove(tablename);
				stmt.close();
			}
		}
		} catch(Exception e){
			System.out.println("Error in reading data while determining PI");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			System.exit(0);
		}
	    c = ctemp;
		return c;
	}
}
