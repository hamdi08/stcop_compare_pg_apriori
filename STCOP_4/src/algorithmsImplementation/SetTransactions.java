package algorithmsImplementation;

import java.sql.Connection;
//import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
//import java.util.Formatter;
//import java.util.Iterator;
import java.util.List;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

//import java.io.BufferedWriter;
import java.io.FileWriter;
//import java.io.FilterWriter;

public class SetTransactions {
	public static List<String> generateTableInstances()
	{
		System.out.println("Inside generateTableInstances...");
		int k = 1;
		List<String> c = new ArrayList<String>();
		List<String> final_pattern_base = new ArrayList<String>();
		
		
		//Initializing current pattern base
		int size1Candidate = 97; // Initial candidates are 'a', 'b', 'c' and so on. 				
		for(int i=0;i<Constants.numberOfEventTypes;i++)
		{
			c.add(i, Character.toString(((char)(size1Candidate + i))));
		}
		
		while(c.size()>0)
		{
			c = Utilities.selfjoin(c, c, final_pattern_base, k);
			if(c.size()>0)
			{
				UpdateTableInstancesWithJoins.updateTables(c, k);
				c = UpdateTableInstancesWithJoins.removeEmptyTablesAndUpdateCandidateBase(c);
			}
			final_pattern_base.addAll(c);
			k = k + 1;
		}
		return final_pattern_base;
	}
	public static void unionAllTableInstances(List<String> p)
	{
		System.out.println("Inside unionAllTableInstances...");
		Connection con = null;
		Statement stmt = null;	
	    String sql;
	    
	    try {
	    	con = DBConnection.getConnection();
	    	stmt = con.createStatement();
	    	sql = "DROP TABLE IF EXISTS st_transactions;";
	    	stmt.execute(sql);
	    	sql = "CREATE TABLE st_transactions(ins_arr BIGINT[], type_arr char[], gen_time BIGINT);";
	    	stmt.execute(sql);
	    	for (int i = 0; i < p.size(); i++) {
				String tablename = p.get(i);
				sql = "DROP TABLE IF EXISTS temp;";
				stmt.execute(sql);
				sql = "CREATE TABLE temp AS SELECT ins_arr, gen_time FROM \""+tablename+"\";";
				stmt.execute(sql);
				sql = "ALTER TABLE temp ADD COLUMN type_arr char[];";
				stmt.execute(sql);
				sql = "UPDATE temp SET type_arr = REGEXP_SPLIT_TO_ARRAY('"+tablename+"','','');";
				stmt.execute(sql);
				sql = "INSERT INTO st_transactions(ins_arr, type_arr, gen_time) "
						+ "SELECT ins_arr, type_arr, gen_time FROM temp;";
				stmt.execute(sql);
				sql = "DROP TABLE "+ tablename +";";
				stmt.execute(sql);
			}
	    	sql = "ALTER TABLE st_transactions ADD COLUMN id BIGSERIAL;";
			stmt.execute(sql);
			sql = "DROP TABLE IF EXISTS temp;";
			stmt.execute(sql);
			
		} catch (Exception e) {
			System.out.println("Error in union tables");
			e.printStackTrace();
			System.exit(0);
		}
	}
	public static void deleteDuplicateTransactions()
	{
		System.out.println("Inside deleteDuplicateTransactions...");
		Connection con = null;
		Statement stmt = null;	
	    String sql;
	    try {
	    	con = DBConnection.getConnection();
	    	stmt = con.createStatement();
	    	sql = "DELETE FROM st_transactions WHERE id IN "
		    		+ "(SELECT s1.id FROM st_transactions AS s1, st_transactions AS s2 "
		    		+ "WHERE s1.id <> s2.id AND "
		    		+ "s1.gen_time = s2.gen_time AND "
		    		+ "s1.ins_arr <> s2.ins_arr AND "
		    		+ "s1.ins_arr <@ s2.ins_arr);";
		    stmt.execute(sql);
		    sql = "ALTER TABLE st_transactions DROP COLUMN id; ";
		    stmt.execute(sql);
		    sql = "ALTER TABLE st_transactions ADD COLUMN id BIGSERIAL;";
		    stmt.execute(sql);
		    sql = "ALTER TABLE st_transactions ADD PRIMARY KEY (id);";
		    stmt.execute(sql);
		   
		    stmt.close();
		} catch (Exception e) {
			System.out.println("Error in deleteDuplicateTransactions");
			e.printStackTrace();
			System.exit(0);
		}
	}
	public static void makeTransactionFile()
	{
		System.out.println("Inside makeTransactionFile");
		Connection con = null;
	    try{
	    	con = DBConnection.getConnection();
			
			CopyManager copyManager = new CopyManager((BaseConnection) con);
			FileWriter fw = new FileWriter("/home/hamdi/PG_files/transaction");
			copyManager.copyOut("COPY (SELECT ARRAY_TO_STRING(ARRAY(SELECT DISTINCT UNNEST(type_arr) ORDER BY 1), ' ') FROM st_transactions) TO STDOUT", fw);
			System.out.println("COPY DONE");
			fw.close();
			    	
		}catch(Exception e){
			System.out.println("Error in makeTransactionFile");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			System.exit(0);
		}

	}

}
