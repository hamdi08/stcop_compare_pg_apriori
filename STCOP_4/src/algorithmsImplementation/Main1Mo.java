package algorithmsImplementation;

import java.io.FileWriter;
import java.util.ArrayList;
//import java.util.Formatter;
import java.util.List;


public class Main1Mo {
	public static void main(String[] args) {
		//double[] PI_base = {0.2, 0.15, 0.1, 0.05};
		//double[] CCE_base = {0.1, 0.0316227766, 0.01, 0.00316227766};
//		double[] PI_base = {0.04, 0.08, 0.12, 0.16, 0.20};
//		double[] CCE_base = {0.01};
		double[] PI_base = {0.001, 0.05, 0.1, 0.15, 0.2};
		double[] CCE_base = {0.01};
		//Formatter output_3_months;
		FileWriter output_1_months;
		
		try {
			output_1_months = new FileWriter("/home/hamdi/STCOP_compare_results/mon_1.csv");
			Constants.FILE_PATH = "/home/hamdi/STCOP_data/data/1Mo/"; //change it according to file system
			Constants.dbName = "real_month";
			//output_3_months.format("%s\n\n", "6 Months data");
			output_1_months.append("pi_th");
			output_1_months.append(",");
			output_1_months.append("cce_th");
			output_1_months.append(",");
			output_1_months.append("runtime_apriori");
			output_1_months.append(",");
			output_1_months.append("cce_call_apriori");
			output_1_months.append(",");
			output_1_months.append("cce_pats_apriori");
			output_1_months.append(",");
			output_1_months.append("runtime_pg");
			output_1_months.append(",");
			output_1_months.append("cce_call_pg");
			output_1_months.append(",");
			output_1_months.append("cce_pats_pg");
			output_1_months.append("\n");
			System.out.printf("%s\n\n", "1 Month data");
			
			DBConnection.establishConnnectionNoGUI();	
			WKTReaderMaker.initializeWKTReader();
			Constants.initializeConsantsParameters();
			Utilities.setSpatialIntersectionExceptionHandler();
			if (! Constants.dbExists) 
			{
				long loadStart = System.currentTimeMillis();
				LoadDataInTablesFromFiles.initializeData();
				List<String> possiblePatterns = new ArrayList<String>();
				possiblePatterns = SetTransactions.generateTableInstances();
				SetTransactions.unionAllTableInstances(possiblePatterns);
				SetTransactions.deleteDuplicateTransactions();
				long loadEnd = System.currentTimeMillis();
				long loadTime = loadEnd - loadStart;
				System.out.println("Data load time (with transactionalizing) from "+ Constants.FILE_PATH + " is "+ (double)loadTime/1000 +" seconds" );
			}
			System.out.println(Constants.dbName+ " exists with st_transactions and a to f tables... starting programs");
			Constants.setNumberOfInstances();
			//call functions here
			//DBConnection.closeConnection();
			for(int i=0;i<PI_base.length;i++)
				for(int j=0;j<CCE_base.length;j++)
				{
					Constants.PI_TH = PI_base[i];
					Constants.CCE_TH = CCE_base[j];
					//output_3_months.format("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					output_1_months.append(String.valueOf(Constants.PI_TH));
					output_1_months.append(",");
					output_1_months.append(String.valueOf(Constants.CCE_TH));
					output_1_months.append(",");
					System.out.printf("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					
					
					//output_3_months.format("%s\n", "FastSTCOP_Apriori");
					System.out.printf("%s\n", "FastSTCOP_Apriori");
					long programStartAP = System.currentTimeMillis();
					FastSTCOP_Apriori.FastSTCOPCaller();
					long programEndAP = System.currentTimeMillis();
					long programTimeAP = programEndAP - programStartAP;
					
					
					//output_3_months.format("Runtime: %f seconds\n", (double)programTimeAP/1000);
					output_1_months.append(String.valueOf((double)programTimeAP/1000));
					output_1_months.append(",");
					System.out.printf("Runtime: %f seconds\n", (double)programTimeAP/1000);
					
					
					//output_3_months.format("Number of CCE calculations: %d\n", Constants.callToJ);
					output_1_months.append(String.valueOf(Constants.callToJ));
					output_1_months.append(",");
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					
					
					//output_3_months.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					output_1_months.append(String.valueOf(Constants.numberOfPatternsForCCETest));
					output_1_months.append(",");
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					
					//output_3_months.format("%s\n", "Pattern Growth Approach");
					System.out.printf("%s\n", "Pattern Growth Approach");
					long programStartPG = System.currentTimeMillis();
					PatternGrowthSTCOP.PGCaller();
					long programEndPG = System.currentTimeMillis();
					long programTimePG = programEndPG - programStartPG;
					
					
					//output_3_months.format("Runtime: %f seconds\n", (double)programTimePG/1000);
					output_1_months.append(String.valueOf((double)programTimePG/1000));
					output_1_months.append(",");
					System.out.printf("Runtime: %f seconds\n", (double)programTimePG/1000);
					
					
					//output_3_months.format("Number of CCE calculations: %d\n", Constants.callToJ);
					output_1_months.append(String.valueOf(Constants.callToJ));
					output_1_months.append(",");
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					
					
					//output_3_months.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					output_1_months.append(String.valueOf(Constants.numberOfPatternsForCCETest));
					output_1_months.append("\n");
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
				}
			DBConnection.closeConnection();
			output_1_months.flush();
			output_1_months.close();
		} catch (Exception e) {
			e.printStackTrace();
		}



	}


}
