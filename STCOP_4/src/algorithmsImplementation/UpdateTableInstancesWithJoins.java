package algorithmsImplementation;

import java.sql.*;  
import java.util.*;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.io.*;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;

/*
 * Written by: Shah Muhammad Hamdi
 * Date: 12/23/2015
 */
public class UpdateTableInstancesWithJoins {
	public static HashMap<String,ArrayList<String>> hm1 = new HashMap<String, ArrayList<String>>();
	public static HashMap<String,Integer> hm2 = new HashMap<String,Integer>();
	
	public static void updateTables(List<String> c, int k)
	{
		Connection con = null ;
	    
	    String sql;
	    /*Connecting to the database*/
	    try{
	    con = DBConnection.getConnection(); 

	    for(int i=0; i<c.size();i++)
	    {
	    	if(k == 1)
	    	{
	    		Statement stmt = null ;
	    		stmt = con.createStatement();
	    		
	    		String tablename = c.get(i);
	    		System.out.println("Creating table "+tablename);
	    		String t1 = Character.toString(tablename.charAt(0));
	    		String t2 = Character.toString(tablename.charAt(1));
	    		
	    		sql = "DROP TABLE IF EXISTS \""+tablename+"\";";
	    		stmt.execute(sql);	    		
	    		sql = "CREATE TABLE \"" +tablename+ "\" AS "
	    			 + "SELECT \""+t1+"\".instance_id AS ins1, \""+t2+"\".instance_id AS ins2, \""+t1+"\".generation_time AS gen_time, \""
	    			 + t1 +"\".polygon_coordinates AS poly1, \""+t2+"\".polygon_coordinates AS poly2 "
	    			 + "FROM \""+t1+"\", \""+t2+"\" "
	    			 + "WHERE \""+t1+"\".generation_time = \""+t2+"\".generation_time AND "
	    			 + "ST_Intersects(\""+t1+"\".polygon_coordinates, \"" +t2+"\".polygon_coordinates) = TRUE;";
	    		stmt.execute(sql);
	    		sql ="ALTER TABLE \"" +tablename+ "\" ADD COLUMN ins_arr BIGINT[];";
	    		stmt.execute(sql);
	    		sql ="SELECT AddGeometryColumn('"+tablename+"', 'intersection_geom', 4326, 'MultiPolygon', 2);";
	    		stmt.execute(sql);
	    		sql = "UPDATE \"" + tablename + "\" SET "
	    				+ "ins_arr = ARRAY [ins1,ins2];";
	    		stmt.executeUpdate(sql);
	    		try{
	    			sql = "UPDATE \"" + tablename + "\" SET "
		    			+ "intersection_geom = intersection_with_exception_handler(poly1, poly2); ";
		    		stmt.executeUpdate(sql);
		    		sql = "DELETE FROM \""+tablename+"\" WHERE intersection_geom IS NULL;";
		    		stmt.execute(sql);
	    		}catch(Exception e){
	    			//Multilinestring problem
	    			System.out.println("Multilinestring problem...");
	    			e.printStackTrace();
	    			System.out.println("Changing intersection_geom column type from multipolygon to geometry ");
	    			sql = "ALTER TABLE \""+tablename+"\" ALTER COLUMN intersection_geom SET DATA TYPE geometry;";
	    			stmt.execute(sql);
	    			System.out.println("Now running update column");
	    			sql = "UPDATE \"" + tablename + "\" SET "
			    			+ "intersection_geom = intersection_with_exception_handler(poly1, poly2); ";
		    		stmt.executeUpdate(sql);
		    		System.out.println("Deleting null geom rows");
		    		sql = "DELETE FROM \""+tablename+"\" WHERE intersection_geom IS NULL;";
		    		stmt.execute(sql);
		    		System.out.println("Deleting all rows whose geometries are not multipolygon");
		    		sql = "DELETE FROM \""+tablename+"\" WHERE ST_AsText(intersection_geom) NOT LIKE 'MULTIPOLYGON%';";
		    		stmt.execute(sql);
		    		System.out.println("Changing intersection_geom column type back to multipolygon");
		    		sql = "ALTER TABLE \""+tablename+"\" ALTER COLUMN intersection_geom SET DATA TYPE geometry(MultiPolygon) USING ST_Multi(intersection_geom);";
		    		stmt.execute(sql);		
	    		}
	    		sql = "ALTER TABLE \""+tablename+ "\" DROP COLUMN ins1, DROP COLUMN ins2, DROP COLUMN poly1, DROP COLUMN poly2;";
	    		stmt.execute(sql);
	    		sql = "ALTER TABLE \""+tablename+"\" ADD CONSTRAINT pk_"+tablename+" PRIMARY KEY (ins_arr, gen_time);";
	    		stmt.execute(sql);
	    		
	    		ArrayList<String> al = new ArrayList<String>();
	    		al.add(t1);
	    		al.add(t2);
	    		hm1.put(tablename, al);
	    		
	    		Statement stmt1 = null;
	    		stmt1 = con.createStatement();
	    		ResultSet rs1;
	    		sql = "SELECT COUNT(*) FROM \""+tablename+"\";";
	    		rs1 = stmt1.executeQuery(sql);
	    		rs1.next();
	    		hm2.put(tablename, rs1.getInt(1));
	    		stmt1.close();
	    		rs1.close();
	    		
	    		stmt.close();

	    	}
	    		
	    	else
	    	{
	    		Statement stmt = null;
	    		stmt = con.createStatement();
	    		
	    		String tablename = c.get(i);
	    		System.out.println("Creating table "+tablename);
	    		String[] smallTables = Utilities.getTwoSmallTables(hm2, tablename, k); 
	    		String t1 = smallTables[0];
	    		String t2 = smallTables[1];
	    		String temp;
	    		
	    		if(t2.compareTo(t1) < 0)
	    		{
	    			temp = t1;
	    			t1 = t2;
	    			t2 = temp;
	    		}
	    		
	    		
	    		sql = "DROP TABLE IF EXISTS \"" + tablename + "\";";
	    		stmt.execute(sql);
	    		sql = "CREATE TABLE \"" + tablename + "\" AS "
	    				+ "SELECT \"" + t1 + "\".ins_arr AS ins1, \"" + t2 + "\".ins_arr AS ins2, \"" + t1 + "\".gen_time AS gen_time, \""
	    				+ t1 + "\".intersection_geom AS poly_i1, \"" + t2 + "\".intersection_geom AS poly_i2 "
	    				+ "FROM \"" + t1 + "\", \"" + t2 + "\" "
	    				+ "WHERE ";
	    		for(int p=0;p<t1.length();p++)
	    			for(int q=0;q<t2.length();q++)
	    				if(t1.charAt(p)==t2.charAt(q))
	    					sql += "\""+ t1+"\".ins_arr["+(int)(p+1)+"]"+" = \""+t2+"\".ins_arr["+(int)(q+1)+"]"+ " AND ";
	    		sql += "\""+ t1 + "\".gen_time = \"" + t2 + "\".gen_time AND "
	    				+ "ST_Intersects(\"" + t1 +"\".intersection_geom, \"" + t2 + "\".intersection_geom) = TRUE;";
	    		stmt.execute(sql);
	    		sql = "ALTER TABLE \"" +tablename+ "\" ADD COLUMN ins_arr BIGINT[];";
	    		stmt.execute(sql);
	    		sql = "SELECT AddGeometryColumn('" + tablename + "', 'intersection_geom', 4326, 'MultiPolygon', 2);";
	    		stmt.execute(sql);
	    		
	    		//Updating ins_arr
	    		ResultSet rs;
	    		Statement st1;
	    		st1 = con.createStatement();
	    		sql = "SELECT DISTINCT ins1, ins2 FROM \""+tablename+"\";";
	    		rs = st1.executeQuery(sql);
	    		while(rs.next())
	    		{
	    			Array arr1 = rs.getArray("ins1");
	    			Long[] a1 = (Long[])arr1.getArray();
	    			Array arr2 = rs.getArray("ins2");
	    			Long[] a2 = (Long[])arr2.getArray();
	    			Long[] arr = Utilities.merge(tablename, t1, a1, t2, a2);
	    			sql = "UPDATE \""+tablename+"\" SET ins_arr = '"+Utilities.getFormattedArray(arr)+"' "
	    				+ "WHERE ins1 = '"+Utilities.getFormattedArray(a1)+"' AND ins2 = '"+Utilities.getFormattedArray(a2)+"';";
	    			stmt.executeUpdate(sql);
	    		}
	    		st1.close();
	    		rs.close();
	    		//Updating intersection_geom
	    		try{
	    		sql = "UPDATE \"" + tablename + "\" SET "
	    				+ "intersection_geom = intersection_with_exception_handler(poly_i1, poly_i2); ";
	    		stmt.executeUpdate(sql);
	    		sql = "DELETE FROM \""+tablename+"\" WHERE intersection_geom IS NULL;";
	    		stmt.execute(sql);
	    		} catch(Exception e){
	    			//Multilinestring problem
	    			System.out.println("Multilinestring problem for table: "+tablename);
	    			e.printStackTrace();
	    			System.out.println("Changing intersection_geom column type from multipolygon to geometry ");
	    			sql = "ALTER TABLE \""+tablename+"\" ALTER COLUMN intersection_geom SET DATA TYPE geometry;";
	    			stmt.execute(sql);
	    			System.out.println("Now running update column");
	    			sql = "UPDATE \"" + tablename + "\" SET "
			    			+ "intersection_geom = intersection_with_exception_handler(poly_i1, poly_i2); ";
		    		stmt.executeUpdate(sql);
		    		System.out.println("Deleting null geom rows");
		    		sql = "DELETE FROM \""+tablename+"\" WHERE intersection_geom IS NULL;";
		    		stmt.execute(sql);
		    		System.out.println("Deleting all rows whose geometries are not multipolygon");
		    		sql = "DELETE FROM \""+tablename+"\" WHERE ST_AsText(intersection_geom) NOT LIKE 'MULTIPOLYGON%';";
		    		stmt.execute(sql);
		    		System.out.println("Changing intersection_geom column type back to multipolygon");
		    		sql = "ALTER TABLE \""+tablename+"\" ALTER COLUMN intersection_geom SET DATA TYPE geometry(MultiPolygon) USING ST_Multi(intersection_geom);";
		    		stmt.execute(sql);		
	    		}
	    		sql = "ALTER TABLE \"" + tablename + "\" DROP COLUMN ins1, DROP COLUMN ins2, DROP COLUMN poly_i1, DROP COLUMN poly_i2;";
	    		stmt.execute(sql);
	    		sql = "ALTER TABLE \"" + tablename + "\" ADD CONSTRAINT pk_" + tablename + " PRIMARY KEY (ins_arr, gen_time);";
	    		stmt.execute(sql);

	    		
	    		ArrayList<String> al = new ArrayList<String>();
	    		al.add(t1);
	    		al.add(t2);
	    		hm1.put(tablename, al);
	    		
	    		Statement stmt1 = null;
	    		stmt1 = con.createStatement();
	    		ResultSet rs1;
	    		sql = "SELECT COUNT(*) FROM \"" + tablename + "\";";
	    		rs1 = stmt1.executeQuery(sql);
	    		rs1.next();
	    		hm2.put(tablename, rs1.getInt(1));
	    		stmt1.close();
	    		rs1.close();
	    		
	    		stmt.close();	    		
	    		
	    	}
	    }
	    
	    } catch (Exception e) { 
	    System.out.println("Error in creating tables");
	    System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	    System.exit(0);
	    }
	}
	public static void specialIntersectionHandler(String tablename)
	{
		try{
			System.out.println("Invoking specialIntersectionHandler...");
			Connection con = DBConnection.getConnection();
			Statement stmt, stmt1;
			stmt = con.createStatement();
			stmt1 = con.createStatement();
			ResultSet rs;
			String sql;
			
			sql = "SELECT DropGeometryColumn('"+tablename+"', 'intersection_geom');";
			stmt.execute(sql);
			sql ="SELECT AddGeometryColumn('"+tablename+"', 'intersection_geom', 4326, 'MultiPolygon', 2);";
			stmt.execute(sql);
    			
			sql = "SELECT ins1, ins2, gen_time, ST_AsText(poly1) AS poly1, ST_AsText(poly2) AS poly2 FROM \""+tablename+"\";";
			rs = stmt1.executeQuery(sql);
			WKTReader wk = WKTReaderMaker.getWktReader();
			while(rs.next())
			{
				Long ins1 = rs.getLong("ins1");
				Long ins2 = rs.getLong("ins2");
				Long gen_time = rs.getLong("gen_time");
				Geometry poly1, poly2, polyI;
				polyI = null;
				poly1 = wk.read(rs.getString("poly1"));
				poly2 = wk.read(rs.getString("poly2"));
				if(!poly1.isValid())
				{
					poly1 = poly1.convexHull();
					poly1 = DouglasPeuckerSimplifier.simplify(poly1, 1);
				}
				if(!poly2.isValid())
				{
					poly2 = poly2.convexHull();
					poly2 = DouglasPeuckerSimplifier.simplify(poly2, 1);
				}
				try{
					polyI = poly1.intersection(poly2);
					if(!polyI.isValid() ||  polyI.getGeometryType().compareTo("Polygon") != 0)
					{
						polyI = polyI.convexHull();
						polyI = DouglasPeuckerSimplifier.simplify(polyI, 1);
					}
					sql = "UPDATE \""+tablename+"\" SET intersection_geom = ST_Multi(ST_GeomFromText('"+polyI+"', 4326)) "
							+ "WHERE ins1 = "+ins1+ " AND ins2 = "+ins2+" AND gen_time = "+gen_time +";";
					stmt.executeUpdate(sql);
				}catch(Exception e){
					sql = "DELETE FROM \""+tablename+"\" "
						+ "WHERE ins1 = "+ins1+ " AND ins2 = "+ins2+" AND gen_time = "+gen_time +";";
					stmt.executeUpdate(sql);
					continue;
				}
				
			}
			stmt1.close();
			rs.close();
			stmt.close();
		}catch(Exception e){
			System.out.println("Some Generic Error in specialIntersectionHandler");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		    System.exit(0);
		}
	}
	public static void specialIntersectionHandlerForLargeTables(String tablename)
	{
		try{
			System.out.println("Invoking specialIntersectionHandlerForLargeTables...");
			Connection con = DBConnection.getConnection();
			Statement stmt, stmt1;
			stmt = con.createStatement();
			stmt1 = con.createStatement();
			ResultSet rs;
			String sql;
			
			sql = "SELECT DropGeometryColumn('"+tablename+"', 'intersection_geom');";
			stmt.execute(sql);
			sql ="SELECT AddGeometryColumn('"+tablename+"', 'intersection_geom', 4326, 'MultiPolygon', 2);";
			stmt.execute(sql);
    			
			sql = "SELECT ins_arr, gen_time, ST_AsText(poly_i1) AS poly_i1, ST_AsText(poly_i2) AS poly_i2 FROM \""+tablename+"\";";
			rs = stmt1.executeQuery(sql);
			WKTReader wk = WKTReaderMaker.getWktReader();
			while(rs.next())
			{
				Array arr = rs.getArray("ins_arr");
    			Long[] ins_arr = (Long[])arr.getArray();
				Long gen_time = rs.getLong("gen_time");
				Geometry poly_i1, poly_i2, polyI;
				polyI = null;
				poly_i1 = wk.read(rs.getString("poly_i1"));
				poly_i2 = wk.read(rs.getString("poly_i2"));
				if(!poly_i1.isValid() || poly_i1.getGeometryType().compareTo("Polygon") != 0)
				{
					poly_i1 = poly_i1.convexHull();
					poly_i1 = DouglasPeuckerSimplifier.simplify(poly_i1, 1);
				}
				if(!poly_i2.isValid() || poly_i2.getGeometryType().compareTo("Polygon")!=0)
				{
					poly_i2 = poly_i2.convexHull();
					poly_i2 = DouglasPeuckerSimplifier.simplify(poly_i2, 1);
				}
				try{
					polyI = poly_i1.intersection(poly_i2);
					//
					if(!polyI.isValid() ||  polyI.getGeometryType().compareTo("Polygon") != 0)
					{
						polyI = polyI.convexHull();
						polyI = DouglasPeuckerSimplifier.simplify(polyI, 1);
					}
					sql = "UPDATE \""+tablename+"\" SET intersection_geom = ST_Multi(ST_GeomFromText('"+polyI+"', 4326)) "
							+ "WHERE ins_arr = '"+Utilities.getFormattedArray(ins_arr)+ "' AND gen_time = "+gen_time +";";
					stmt.executeUpdate(sql);
				}catch(Exception e){
					sql = "DELETE FROM \""+tablename+"\" "
						+ "WHERE ins_arr = '"+Utilities.getFormattedArray(ins_arr)+ "' AND gen_time = "+gen_time +";";
					stmt.executeUpdate(sql);
					continue;
				}
				
			}
			stmt1.close();
			rs.close();
			stmt.close();
		}catch(Exception e){
			System.out.println("Some Generic Error in specialIntersectionHandler");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		    System.exit(0);
		}
	}
	public static List<String> removeEmptyTablesAndUpdateCandidateBase(List<String> c)
	{
		Connection con = null ;
		Statement stmt = null;	
	    String sql;
	    List<String> ctemp = new ArrayList<String>();
	    ctemp.addAll(c);
	    try {
	    	con = DBConnection.getConnection();
	    	stmt = con.createStatement();
	    	for (int i = 0; i < c.size(); i++) {
	    		String tablename = c.get(i);
	    		Statement stmt1;
				ResultSet rs1;
				sql = "SELECT COUNT (*) FROM \""+tablename+"\";";
				stmt1 = con.createStatement();
				rs1 = stmt1.executeQuery(sql);
				rs1.next();
				int rowCount = rs1.getInt(1);
				stmt1.close();
				rs1.close();
	    		
				if (rowCount == 0) {
					sql = "DROP TABLE \""+tablename+"\" CASCADE;";
					stmt.execute(sql);
					hm2.remove(tablename);
					ctemp.remove(tablename);
				} 
			}
			
		} catch (Exception e) {
			System.out.println("Error in removeEmptyTables");
			e.printStackTrace();
			System.exit(0);
		}
	    c = ctemp;
		return c;
	}
	

}
