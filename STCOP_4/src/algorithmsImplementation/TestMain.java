package algorithmsImplementation;

import java.io.FileWriter;
import java.util.ArrayList;
//import java.util.Formatter;
import java.util.List;

public class TestMain {

	public static void main(String[] args) {
		double[] PI_base = {0.2, 0.15, 0.1, 0.05};
		double[] CCE_base = {0.1, 0.0316227766, 0.01, 0.00316227766};
		
		FileWriter output_test_arti_4_features, output_real_test_6_features; 
		
		try {
			output_test_arti_4_features = new FileWriter("/home/hamdi/STCOP_compare_results/test_arti_f_4.csv");
			Constants.FILE_PATH = "/home/hamdi/STCOP_data/test_arti/"; //change it according to file system
			Constants.dbName = "test_artificial_4_features";

			
			//output_test_arti_4_features.format("%s\n\n", "Test articial 4 fetures");
			output_test_arti_4_features.append("pi_th");
			output_test_arti_4_features.append(",");
			output_test_arti_4_features.append("cce_th");
			output_test_arti_4_features.append(",");
			output_test_arti_4_features.append("runtime_apriori");
			output_test_arti_4_features.append(",");
			output_test_arti_4_features.append("cce_call_apriori");
			output_test_arti_4_features.append(",");
			output_test_arti_4_features.append("cce_pats_apriori");
			output_test_arti_4_features.append(",");
			output_test_arti_4_features.append("runtime_pg");
			output_test_arti_4_features.append(",");
			output_test_arti_4_features.append("cce_call_pg");
			output_test_arti_4_features.append(",");
			output_test_arti_4_features.append("cce_pats_pg");
			output_test_arti_4_features.append("\n");
			
			
			
			System.out.printf("%s\n\n", "Test articial 4 fetures");
			
			DBConnection.establishConnnectionNoGUI();	
			WKTReaderMaker.initializeWKTReader();
			Constants.initializeConsantsParameters();
			Utilities.setSpatialIntersectionExceptionHandler();
			if (! Constants.dbExists) 
			{
				long loadStart = System.currentTimeMillis();
				LoadDataInTablesFromFiles.initializeData();
				List<String> possiblePatterns = new ArrayList<String>();
				possiblePatterns = SetTransactions.generateTableInstances();
				SetTransactions.unionAllTableInstances(possiblePatterns);
				SetTransactions.deleteDuplicateTransactions();
				long loadEnd = System.currentTimeMillis();
				long loadTime = loadEnd - loadStart;
				System.out.println("Data load time (with transactionalizing) from "+ Constants.FILE_PATH + " is "+ (double)loadTime/1000 +" seconds" );
			}
			
			System.out.println(Constants.dbName+ " exists with st_transactions and a to f tables... starting programs");
			Constants.setNumberOfInstances();
			for(int i=0;i<PI_base.length;i++)
				for(int j=0;j<CCE_base.length;j++)
				{
					Constants.PI_TH = PI_base[i];
					Constants.CCE_TH = CCE_base[j];
					
					//output_test_arti_4_features.format("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					output_test_arti_4_features.append(String.valueOf(Constants.PI_TH));
					output_test_arti_4_features.append(",");
					output_test_arti_4_features.append(String.valueOf(Constants.CCE_TH));
					output_test_arti_4_features.append(",");
							
					
					
					System.out.printf("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					//output_test_arti_4_features.format("%s\n", "FastSTCOP_Apriori");
					System.out.printf("%s\n", "FastSTCOP_Apriori");
					long programStartApriori = System.currentTimeMillis();
					FastSTCOP_Apriori.FastSTCOPCaller();
					long programEndApriori = System.currentTimeMillis();
					long programTimeApriori = programEndApriori - programStartApriori;
					
					//output_test_arti_4_features.format("Runtime: %f seconds\n", (double)programTimeApriori/1000);
					output_test_arti_4_features.append(String.valueOf((double)programTimeApriori/1000));
					output_test_arti_4_features.append(",");
					System.out.printf("Runtime: %f seconds\n", (double)programTimeApriori/1000);
					
					
					//output_test_arti_4_features.format("Number of CCE calculations: %d\n", Constants.callToJ);
					output_test_arti_4_features.append(String.valueOf(Constants.callToJ));
					output_test_arti_4_features.append(",");
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					
					
					
					//output_test_arti_4_features.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					output_test_arti_4_features.append(String.valueOf(Constants.numberOfPatternsForCCETest));
					output_test_arti_4_features.append(",");
					
					
					//output_test_arti_4_features.format("%s\n", "Pattern Growth Approach");
					System.out.printf("%s\n", "Pattern Growth Approach");
					long programStartPG = System.currentTimeMillis();
					PatternGrowthSTCOP.PGCaller();
					long programEndPG = System.currentTimeMillis();
					long programTimePG = programEndPG - programStartPG;
					//output_test_arti_4_features.format("Runtime: %f seconds\n", (double)programTimePG/1000);
					output_test_arti_4_features.append(String.valueOf((double)programTimePG/1000));
					output_test_arti_4_features.append(",");
					System.out.printf("Runtime: %f seconds\n", (double)programTimePG/1000);
					
					
					//output_test_arti_4_features.format("Number of CCE calculations: %d\n", Constants.callToJ);
					output_test_arti_4_features.append(String.valueOf(Constants.callToJ));
					output_test_arti_4_features.append(",");
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					
					
					//output_test_arti_4_features.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					output_test_arti_4_features.append(String.valueOf(Constants.numberOfPatternsForCCETest));
					output_test_arti_4_features.append("\n");
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
				}
			DBConnection.closeConnection();
			output_test_arti_4_features.flush();
			output_test_arti_4_features.close();
			
			
			
			
			output_real_test_6_features = new FileWriter("/home/hamdi/STCOP_compare_results/test_real_small_f_6.csv");
			Constants.FILE_PATH = "/home/hamdi/STCOP_data/test_features/"; //change it according to file system
			Constants.dbName = "test_real_6_features";
			//output_real_test_6_features.format("%s\n\n", "Test real 6 fetures");
			output_real_test_6_features.append("pi_th");
			output_real_test_6_features.append(",");
			output_real_test_6_features.append("cce_th");
			output_real_test_6_features.append(",");
			output_real_test_6_features.append("runtime_apriori");
			output_real_test_6_features.append(",");
			output_real_test_6_features.append("cce_call_apriori");
			output_real_test_6_features.append(",");
			output_real_test_6_features.append("cce_pats_apriori");
			output_real_test_6_features.append(",");
			output_real_test_6_features.append("runtime_pg");
			output_real_test_6_features.append(",");
			output_real_test_6_features.append("cce_call_pg");
			output_real_test_6_features.append(",");
			output_real_test_6_features.append("cce_pats_pg");
			output_real_test_6_features.append("\n");
			System.out.printf("%s\n\n", "Test real 6 fetures");
			
			DBConnection.establishConnnectionNoGUI();	
			WKTReaderMaker.initializeWKTReader();
			Constants.initializeConsantsParameters();
			Utilities.setSpatialIntersectionExceptionHandler();
			if (! Constants.dbExists) 
			{
				long loadStart = System.currentTimeMillis();
				LoadDataInTablesFromFiles.initializeData();
				List<String> possiblePatterns = new ArrayList<String>();
				possiblePatterns = SetTransactions.generateTableInstances();
				SetTransactions.unionAllTableInstances(possiblePatterns);
				SetTransactions.deleteDuplicateTransactions();
				long loadEnd = System.currentTimeMillis();
				long loadTime = loadEnd - loadStart;
				System.out.println("Data load time (with transactionalizing) from "+ Constants.FILE_PATH + " is "+ (double)loadTime/1000 +" seconds" );
			}
			
			System.out.println(Constants.dbName+ " exists with st_transactions and a to f tables... starting programs");
			Constants.setNumberOfInstances();
			//call functions here
			//DBConnection.closeConnection();
			for(int i=0;i<PI_base.length;i++)
				for(int j=0;j<CCE_base.length;j++)
				{
					Constants.PI_TH = PI_base[i];
					Constants.CCE_TH = CCE_base[j];
					
					
					//output_real_test_6_features.format("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					output_real_test_6_features.append(String.valueOf(Constants.PI_TH));
					output_real_test_6_features.append(",");
					output_real_test_6_features.append(String.valueOf(Constants.CCE_TH));
					output_real_test_6_features.append(",");
					System.out.printf("PI_TH = %.3f\tCCE_TH = %.3f\n", Constants.PI_TH, Constants.CCE_TH);
					
					
					//output_real_test_6_features.format("%s\n", "FastSTCOP_Apriori");
					System.out.printf("%s\n", "FastSTCOP_Apriori");
					long programStartApriori = System.currentTimeMillis();
					FastSTCOP_Apriori.FastSTCOPCaller();
					long programEndApriori = System.currentTimeMillis();
					long programTimeApriori = programEndApriori - programStartApriori;
					
					
					//output_real_test_6_features.format("Runtime: %f seconds\n", (double)programTimeApriori/1000);
					output_real_test_6_features.append(String.valueOf((double)programTimeApriori/1000));
					output_real_test_6_features.append(",");
					System.out.printf("Runtime: %f seconds\n", (double)programTimeApriori/1000);
					
					
					//output_real_test_6_features.format("Number of CCE calculations: %d\n", Constants.callToJ);
					output_real_test_6_features.append(String.valueOf(Constants.callToJ));
					output_real_test_6_features.append(",");
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					
					
					//output_real_test_6_features.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					output_real_test_6_features.append(String.valueOf(Constants.numberOfPatternsForCCETest));
					output_real_test_6_features.append(",");
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					
					//output_real_test_6_features.format("%s\n", "Pattern Growth Approach");
					System.out.printf("%s\n", "Pattern Growth Approach");
					long programStartPG = System.currentTimeMillis();
					PatternGrowthSTCOP.PGCaller();
					long programEndPG = System.currentTimeMillis();
					long programTimePG = programEndPG - programStartPG;
					
					
					//output_real_test_6_features.format("Runtime: %f seconds\n", (double)programTimePG/1000);
					output_real_test_6_features.append(String.valueOf((double)programTimePG/1000));
					output_real_test_6_features.append(",");
					System.out.printf("Runtime: %f seconds\n", (double)programTimePG/1000);
					
					
					//output_real_test_6_features.format("Number of CCE calculations: %d\n", Constants.callToJ);
					output_real_test_6_features.append(String.valueOf(Constants.callToJ));
					output_real_test_6_features.append(",");
					System.out.printf("Number of CCE calculations: %d\n", Constants.callToJ);
					
					
					//output_real_test_6_features.format("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
					output_real_test_6_features.append(String.valueOf(Constants.numberOfPatternsForCCETest));
					output_real_test_6_features.append("\n");
					System.out.printf("Number of patterns whose CCE is calculated: %d\n", Constants.numberOfPatternsForCCETest);
				}
			DBConnection.closeConnection();
			output_real_test_6_features.flush();
			output_real_test_6_features.close();
		} catch (Exception e) {
			System.out.println("Formatter problem");
			e.printStackTrace();
		}

	}

}
