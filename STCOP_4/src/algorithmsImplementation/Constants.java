package algorithmsImplementation;
import java.io.*;
import java.sql.*;
/*
 * Written by: Shah Muhammad Hamdi
 * Date: 12/20/2015
 */

public class Constants {
	public static int numberOfEventTypes;
	public static String FILE_PATH;
	public static double CCE_TH;
	public static double PI_TH;
	public static String[] eventTypes;
	public static File[] files;
	public static int[] no_of_instances;
	public static Long deltaT;
	public static int callToJ;
	public static String dbName;
	public static boolean dbExists;
	public static int numberOfPatternsForCCETest;
	//15 features 12 MB
	//public static final String FILE_PATH ="F:\\STCOP_data_directory\\6K_instances\\";
	//4 features of 6k/home/hamdi/STCOP_data/6k2
	//public static final String FILE_PATH ="F:\\STCOP_data_directory\\6K2\\";
	//9 features 315 MB
	//public static final String FILE_PATH ="F:\\STCOP_data_directory\\output1417900034638\\";
	//Real Data 1 month
	//public static String FILE_PATH ="F:\\STCOP_data_directory\\data\\1Mo\\";
	//Real Data 3 month
	//public static final String FILE_PATH ="F:\\STCOP_data_directory\\data\\3Mo\\";
	//Real Data 6 month
	//public static final String FILE_PATH ="F:\\STCOP_data_directory\\data\\6Mo\\";
	//Real Data 12 month
	//public static final String FILE_PATH ="F:\\STCOP_data_directory\\data\\12Mo\\";
	//Subset of 1 month real data
	//public static final String FILE_PATH ="F:\\STCOP_data_directory\\data\\subset\\";
	//F:\STCOP_data_directory\test features
	//public static final String FILE_PATH = "F:\\STCOP_data_directory\\test features\\";
	//Artificial small set of 4 event types
	//public static final String FILE_PATH = "F:\\STCOP_data_directory\\test arti\\";
	//public static final double CCE_TH = 0.02;
	//public static final double PI_TH = 0.05;

	
	public static void setEventTypes(File[] files)
	{
		eventTypes = new String[files.length];
		String fileName;
		for(int i =0;i<files.length;i++)
		{
			fileName = files[i].getName();
			if(fileName.contains(".txt"))
				eventTypes[i] = fileName.substring(0, 2);	
			else
				eventTypes[i] = fileName;
		}
		no_of_instances = new int[files.length];
	}
	public static void setNoOfEventTypes(int n)
	{
		numberOfEventTypes = n;
	}
	public static void setDeltaT(Long value)
	{
		deltaT = value;
	}
	public static Long getDeltaT()
	{
		return deltaT;
	}
	public static void initializeConsantsParameters()
	{
	    File datasetFolder = new File(Constants.FILE_PATH);
	    files = datasetFolder.listFiles();
	    Constants.setEventTypes(files);
	    int numberOfEvents = files.length;
	    Constants.setNoOfEventTypes(numberOfEvents);
	    
	    //Setting delta T
	    try{
	    BufferedReader b1 = new BufferedReader(new FileReader(files[0]));
	    
	    String l1 =  b1.readLine();
	    String[] v1 = l1.split("\\t");
	    String l2 =  b1.readLine();
	    String[] v2 = l2.split("\\t");

	    Long sampleTime1 = Long.parseLong(v1[1]);
	    Long sampleTime2 = Long.parseLong(v2[1]);
	    Long sampleInterval = sampleTime2 - sampleTime1; 
	    Constants.setDeltaT(sampleInterval);
	    b1.close();
	    } catch(Exception e){
	    	System.out.println("Error in setting parameters.");
	    	e.printStackTrace();
	    	System.exit(0);
	    }  
	}
	public static void setNumberOfInstances()
	{
		Connection con;
		
		String sql;
		try {
			con = DBConnection.getConnection();
			
			int table_name = 97;
			for (int i = 0; i < Constants.numberOfEventTypes; i++) {
				Statement stmt;
				ResultSet rs;
				stmt = con.createStatement();
				sql = "SELECT COUNT(DISTINCT instance_id) FROM \""+(char)(table_name+i)+"\";";
				rs = stmt.executeQuery(sql);
				rs.next();
				Constants.no_of_instances[i] = rs.getInt(1);
				stmt.close();
				rs.close();
			}
			
		} catch (Exception e) {
			System.out.println("Error in setNumofInstances");
			e.printStackTrace();
			System.exit(0);
		}
	}
	public static void showInputInfo()
	{
		System.out.println("PI_TH: "+PI_TH);
		System.out.println("CCE_TH: "+CCE_TH);
		System.out.println("Delta T: "+ Constants.deltaT);
		System.out.println("Dataset file path: "+ FILE_PATH);
		System.out.println("# of files, i.e. event types: "+ numberOfEventTypes);
		System.out.println("Event types and # of distinct instances: ");
		for(int i=0;i<Constants.eventTypes.length;i++)
			System.out.println(Constants.eventTypes[i]+": "+Constants.no_of_instances[i]);
		
	}
}