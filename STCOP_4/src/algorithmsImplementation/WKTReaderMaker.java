package algorithmsImplementation;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.io.*;

public class WKTReaderMaker {
	private static GeometryFactory fact;
	private static WKTReader wktRdr;
	public static void initializeWKTReader()
	{
		fact = new GeometryFactory();
		wktRdr = new WKTReader(fact);
	}
	public static WKTReader getWktReader()
	{
		return wktRdr;
	}

}
