package algorithmsImplementation;
//import java.io.*;
import java.sql.*;
import java.util.*;


public class PatternAnalyzer {
	public static ArrayList<ArrayList<Long>> JaccardTrash = new ArrayList<ArrayList<Long>>();
	public static Map<String, Set<ArrayList<Long>>> allTableInstances = new HashMap<String, Set<ArrayList<Long>>>();
	static Map<String, Double> refinedProtoPatterns = new HashMap<String, Double>();
	
	public static void refineProtoPatterns()
	{
		System.out.println("Inside refine proto patterns... ");
		for (String pattern: FPGrowthCaller.cleanedProtoPatterns.keySet())
		{
			double PI = getParticipationIndexofProtoPatterns(pattern);
			if(PI>=Constants.PI_TH)
				refinedProtoPatterns.put(pattern, PI);
		}
	}
	
	
	public static void refinePatterns()
	{
		System.out.println("Inside refine patterns...");
		try{
			ArrayList<String> initialAL = new ArrayList<String>();
			int maxPatternLength = 2;
			for(String pattern: refinedProtoPatterns.keySet())
			{
				int patternLength = pattern.length();
				if(patternLength > maxPatternLength)
					maxPatternLength = patternLength;
				//store in a local AL
				initialAL.add(pattern);
			}
			
			
			//Storing the size k patterns in hash map
			HashMap<Integer, ArrayList<String>> hmPP = new HashMap<Integer, ArrayList<String>>();
			for(Integer i = 2; (int)i<=maxPatternLength;i++)
			{
				ArrayList<String> level2AL = new ArrayList<String>();
				//Traverse the initialAL
				Iterator <String> itInitialAL = initialAL.iterator();
				while(itInitialAL.hasNext())
				{
					String leafPattern = itInitialAL.next();
					if(leafPattern.length() == (int)i)
						level2AL.add(leafPattern);
				}
				hmPP.put(i, level2AL);
			}
			List<String> c = new ArrayList<String>();
			List<String> final_pattern_base = new ArrayList<String>();
			
			//Size 2 candidates
			c = hmPP.get(2);
			List<String> ctemp = new ArrayList<String>();
		    ctemp.addAll(c);
			for (String candidate : c) 
			{
				Constants.numberOfPatternsForCCETest ++;
				double PI = getParticipationIndexofPatterns(candidate);
				if(PI < Constants.PI_TH)
					ctemp.remove(candidate);
				
			}
			c.clear();
			c.addAll(ctemp);
			final_pattern_base.addAll(c);
			
			//Generation of size > 2 frequent patterns
			int k = 2;
			while(c.size()>0)
			{
				c = Utilities.selfjoin(c, c, final_pattern_base, k);
				if(hmPP.containsKey(k+1))
				{
					ArrayList<String> SKPP = hmPP.get(k+1);
					c.retainAll(SKPP);
				}
				else
					c.clear();
				if(c.size() > 0)
				{
					//UpdateTableInstancesWithJoins.updateTables(c, k);
					//Filtering existing table instances by Jaccard
					//JFilter.deleteJaccardFailedRows(c, k);
					List<String> ctemp2 = new ArrayList<String>();
				    ctemp2.addAll(c);
					for (String candidate : c) 
					{
						Constants.numberOfPatternsForCCETest ++;
						double PI = getParticipationIndexofPatterns(candidate);
						if(PI < Constants.PI_TH)
							ctemp2.remove(candidate);
						
					}
					c.clear();
					c.addAll(ctemp2);
					//Updating final pattern base
					final_pattern_base.addAll(c);
					k = k + 1;
				}
			}
			System.out.println("No. of patterns: "+ final_pattern_base.size());
			System.out.println("Final STCOPs...");
			System.out.println(final_pattern_base);
			Utilities.showOrginalPatterns(final_pattern_base);
			
		}catch(Exception e){
			System.out.println("Error in updated refine Patterns");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			System.exit(0);
		}

	}
	
	
	public static double getJaccard(char[] features, Long[] insID)
	{
		Constants.callToJ++;
		double J = 0.0;
		Connection con = null;
		String sql;
		try{
			con = DBConnection.getConnection();
			//Calculating time span of all instances
			Long[] minTime = new Long[insID.length];
			Long[] maxTime = new Long[insID.length];
			for(int i=0;i<features.length;i++)
			{
				char tableName = features[i];
				ResultSet rs1, rs2;
				Statement stmt1, stmt2;
				sql = "SELECT MIN(generation_time) FROM \"" + tableName +"\" WHERE instance_id = " + insID[i] + ";";
				stmt1 = con.createStatement();
				rs1 = stmt1.executeQuery(sql);
				rs1.next();
				minTime[i] =rs1.getLong(1);
				stmt1.close();
				rs1.close();
				
				sql = "SELECT MAX(generation_time) FROM \"" + tableName + "\" WHERE instance_id = " + insID[i] + ";";
				stmt2 = con.createStatement();
				rs2 = stmt2.executeQuery(sql);
				rs2.next();
				maxTime[i] =rs2.getLong(1);
				stmt2.close();
				rs2.close();
			}
			Long ultMin = Utilities.getMinLong(minTime);
			Long ultMax = Utilities.getMaxLong(maxTime);
			Long deltaT = Constants.getDeltaT();
			double unionArea = 0.0;
			double intersectionArea = 0.0;
			//Going to each valid timestamp, collecting geometries of instances of different types and getting their union
			//and intersection and summing them to get area; finally multiplying with time to get volume
			for(Long i=ultMin; i<=ultMax; i+= deltaT)
			{
				String[] collectedPolygons = new String[insID.length];
				boolean flag = true;
				for(int j=0;j<features.length;j++)
				{
					char tableName = features[j];
					ResultSet rs;
					Statement stmt;
					stmt = con.createStatement();
					sql = "SELECT ST_AsText(polygon_coordinates) FROM \""+tableName+"\" WHERE instance_id = "+ insID[j] +""
						   + " AND generation_time = "+ i +";";
					
					rs = stmt.executeQuery(sql);
					if(!rs.next())
					{
						flag = false;
						collectedPolygons[j] = "GEOMETRYCOLLECTION EMPTY";
					}
					else
						collectedPolygons[j] = rs.getString(1);
					stmt.close();
					rs.close();
				}
				unionArea = unionArea + SpatialUtilities.getUnionArea(collectedPolygons);
				if(flag == true)
				{
					intersectionArea = intersectionArea + SpatialUtilities.getIntersectionArea(collectedPolygons);
				}
			}
			double unionVolume = unionArea * (double)deltaT;
			double intersectionVolume = intersectionArea * (double)deltaT;
			J = intersectionVolume/unionVolume;
		}catch(Exception e)
		{
			System.out.println("Error in getting Jaccard of two trajectories");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			System.exit(0);
		}
		return J;
	}
	
	public static boolean inTrash(ArrayList<Long> patIns)
	{
		for(ArrayList<Long> x: PatternAnalyzer.JaccardTrash)
			if(patIns.containsAll(x)){
				return true;
			}
		return false;
	}
	
	public static double getParticipationIndexofPatterns(String features)
	{
		
		String featureArrforDB = Arrays.toString(features.toCharArray()).replace('[', '{');
		featureArrforDB = featureArrforDB.replace(']', '}');
		double[] PR = new double[features.length()];

		try{
			double CCE = 0;
			Set<ArrayList<Long>> distinctPatternInstances = allTableInstances.get(features);
			Iterator<ArrayList<Long>> itDistinctPatternInstances = distinctPatternInstances.iterator();
			int patternInstanceCount=0;
			Long[][] instancesInPattern = new Long[distinctPatternInstances.size()][features.length()];
			while(itDistinctPatternInstances.hasNext())
			{
				ArrayList<Long> patternInstanceAL = itDistinctPatternInstances.next();
				//Jaccard code here
				Long[] insID = new Long[patternInstanceAL.size()];
				for(int i=0;i<patternInstanceAL.size();i++)
					insID[i] = patternInstanceAL.get(i);
				//Throw all jaccard failed pattern instances into a global trash
				//so that new pattern instance can be tested whether it or its part is already in the trash
				if(!inTrash(patternInstanceAL))
				{
					CCE = getJaccard(features.toCharArray(), insID);
					
					if(CCE >= Constants.CCE_TH)
					{
						for(int k=0;k<features.length();k++)
							instancesInPattern[patternInstanceCount][k] = insID[k];
						patternInstanceCount++;
					}
					else
						PatternAnalyzer.JaccardTrash.add(patternInstanceAL);
				}
				else 
					PatternAnalyzer.JaccardTrash.add(patternInstanceAL);
			}
			int numOfRowsIn2DArray = patternInstanceCount;
					
			for(int k=0;k<features.length();k++)
			{
				Long[] instancesOfIndividualFeature = new Long[numOfRowsIn2DArray];
				for(int j=0;j<numOfRowsIn2DArray;j++)
					instancesOfIndividualFeature[j] = instancesInPattern[j][k];
				Set<Long> uniqueInstances = new HashSet<Long>();
				uniqueInstances.addAll(Arrays.asList(instancesOfIndividualFeature));
				int numOfUniqueInstancesInPattern = uniqueInstances.size();
				int numOfTotalInstancesOfFeature = Constants.no_of_instances[(int)features.charAt(k) - 97];
				PR[k] = (double)numOfUniqueInstancesInPattern/numOfTotalInstancesOfFeature;
			}
		}catch(Exception e)
		{
			System.out.println("Error in getParticipationIndex");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			System.exit(0);
		}
		double PI = Utilities.getMin(PR);
		return PI;
	}
	
	public static double getParticipationIndexofProtoPatterns(String features)
	{
		String featureArrforDB = Arrays.toString(features.toCharArray()).replace('[', '{');
		featureArrforDB = featureArrforDB.replace(']', '}');
		double[] PR = new double[features.length()];
		String sql;
		Connection con = null;
		try{
			con = DBConnection.getConnection();
			
			Set<ArrayList<Long>> distinctPatternInstances = new HashSet<ArrayList<Long>>();
			Statement st2;
			ResultSet rs2;
			st2 = con.createStatement();
			sql = "SELECT * FROM st_transactions WHERE type_arr @> '"+featureArrforDB+"' ;";
			rs2 = st2.executeQuery(sql);
			while(rs2.next())
			{
				Array type_arrForm1 = rs2.getArray("type_arr");
				String[] tempStrArr = (String[])type_arrForm1.getArray();
				char[] type_arr = new char[tempStrArr.length];
				for (int i = 0; i < type_arr.length; i++) {
					type_arr[i] = tempStrArr[i].charAt(0);
				}
				Arrays.sort(type_arr);
				
				Array ins_id_arr = rs2.getArray("ins_arr");
				Long[] ins_arr = (Long[])ins_id_arr.getArray();
				
				ArrayList<Long> patternInstance = new ArrayList<Long>();
				
				for(int j=0;j<features.length();j++)
				{
					int featurePosition = Arrays.binarySearch(type_arr, features.charAt(j));
					patternInstance.add(ins_arr[featurePosition]);
				}
				distinctPatternInstances.add(patternInstance);	
			}
			st2.close();
			rs2.close();
			allTableInstances.put(features, distinctPatternInstances);
			
			
			//storing the distinct instances of each feature in a set; all such sets form an arraylist of sets
			ArrayList<HashSet<Long>> alForDistinctInstances = new ArrayList<HashSet<Long>>();
			for(int i=0;i<features.length();i++)
			{
				alForDistinctInstances.add(new HashSet<Long>());
			}
			Iterator<ArrayList<Long>> itDistinctPatternInstances = distinctPatternInstances.iterator();
			while (itDistinctPatternInstances.hasNext()) {
				ArrayList<Long> patternInstanceAL = itDistinctPatternInstances.next();
				for(int i=0;i<patternInstanceAL.size();i++)
				{
					Long ins = patternInstanceAL.get(i);
					alForDistinctInstances.get(i).add(ins);
				}
			}
			
			
			for(int i=0;i<features.length();i++)
			{
				int numOfUniqueInstancesInPattern = alForDistinctInstances.get(i).size();
				int numOfTotalInstancesOfFeature = Constants.no_of_instances[(int)features.charAt(i) - 97];
				PR[i] = (double)numOfUniqueInstancesInPattern/numOfTotalInstancesOfFeature;
			}
			

		}catch(Exception e)
		{
			System.out.println("Error in getParticipationIndexofProtoPatterns");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			System.exit(0);
		}
		double PI = Utilities.getMin(PR);
		return PI;
	}
	
	public static double getParticipationIndexofPatternsForApriori(String features)
	{
		String featureArrforDB = Arrays.toString(features.toCharArray()).replace('[', '{');
		featureArrforDB = featureArrforDB.replace(']', '}');
		double[] PR = new double[features.length()];
		String sql;
		Connection con = null;
		try{
			con = DBConnection.getConnection();
			
			Set<ArrayList<Long>> distinctPatternInstances = new HashSet<ArrayList<Long>>();
			Statement st2;
			ResultSet rs2;
			st2 = con.createStatement();
			sql = "SELECT * FROM st_transactions WHERE type_arr @> '"+featureArrforDB+"' ;";
			rs2 = st2.executeQuery(sql);
			double CCE = 0;
			while(rs2.next())
			{
				Array type_arrForm1 = rs2.getArray("type_arr");
				String[] tempStrArr = (String[])type_arrForm1.getArray();
				char[] type_arr = new char[tempStrArr.length];
				for (int i = 0; i < type_arr.length; i++) {
					type_arr[i] = tempStrArr[i].charAt(0);
				}
				Arrays.sort(type_arr);
				
				Array ins_id_arr = rs2.getArray("ins_arr");
				Long[] ins_arr = (Long[])ins_id_arr.getArray();
				
				ArrayList<Long> patternInstance = new ArrayList<Long>();
				
				for(int j=0;j<features.length();j++)
				{
					int featurePosition = Arrays.binarySearch(type_arr, features.charAt(j));
					patternInstance.add(ins_arr[featurePosition]);
				}
				distinctPatternInstances.add(patternInstance);
				
			}
			st2.close();
			rs2.close();
			Iterator<ArrayList<Long>> itDistinctPatternInstances = distinctPatternInstances.iterator();
			int patternInstanceCount=0;
			Long[][] instancesInPattern = new Long[distinctPatternInstances.size()][features.length()];
			while(itDistinctPatternInstances.hasNext())
			{
				ArrayList<Long> patternInstanceAL = itDistinctPatternInstances.next();
				//Jaccard code here
				Long[] insID = new Long[patternInstanceAL.size()];
				for(int i=0;i<patternInstanceAL.size();i++)
					insID[i] = patternInstanceAL.get(i);
				//Throw all jaccard failed pattern instances into a global trash
				//so that new pattern instance can be tested whether it or its part is already in the trash
				if(!inTrash(patternInstanceAL))
				{
					CCE = getJaccard(features.toCharArray(), insID);
					if(CCE >= Constants.CCE_TH)
					{
						for(int k=0;k<features.length();k++)
							instancesInPattern[patternInstanceCount][k] = insID[k];
						patternInstanceCount++;
					}
					else
						PatternAnalyzer.JaccardTrash.add(patternInstanceAL);
				}
				else 
					PatternAnalyzer.JaccardTrash.add(patternInstanceAL);
			}	
			int numOfRowsIn2DArray = patternInstanceCount;
					
			for(int k=0;k<features.length();k++)
			{
				Long[] instancesOfIndividualFeature = new Long[numOfRowsIn2DArray];
				for(int j=0;j<numOfRowsIn2DArray;j++)
					instancesOfIndividualFeature[j] = instancesInPattern[j][k];
				Set<Long> uniqueInstances = new HashSet<Long>();
				uniqueInstances.addAll(Arrays.asList(instancesOfIndividualFeature));
				int numOfUniqueInstancesInPattern = uniqueInstances.size();
				int numOfTotalInstancesOfFeature = Constants.no_of_instances[(int)features.charAt(k) - 97];
				PR[k] = (double)numOfUniqueInstancesInPattern/numOfTotalInstancesOfFeature;
			}
		}catch(Exception e)
		{
			System.out.println("Error in getParticipationIndex of Apriori");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			System.exit(0);
		}
		double PI = Utilities.getMin(PR);
		return PI;
	}

}
