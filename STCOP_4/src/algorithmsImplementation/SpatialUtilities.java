package algorithmsImplementation;
 
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.io.*;

/*
 * Written by: Shah Muhammad Hamdi
 * Date: 12/29/2015
 */
public class SpatialUtilities {
	/*
	
	*/
	public static double getArea(String polygonOrMultiPolygon)
	{
		double area = 0.0;
		try{
		WKTReader wktRdr = WKTReaderMaker.getWktReader();
		Geometry A = wktRdr.read(polygonOrMultiPolygon);
		area = A.getArea();
		} catch(Exception e){
			System.out.println("Error in getting area");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			return 0.0;
		}
		return area;
	}
	
	public static String getUnion(String wktA, String wktB)
	{
		String union = "";
		try{
			WKTReader wktRdr = WKTReaderMaker.getWktReader();
			Geometry A = wktRdr.read(wktA);
			Geometry B = wktRdr.read(wktB);
			Geometry C = A.union(B);
			union = C.toString();
			
		} catch(Exception e){
			System.out.println("Error in getting union");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			return "GEOMETRYCOLLECTION EMPTY";
		}

		return union;
	}
	
	public static double getUnionArea(String[] polygons)
	{
		String UoP, P0, P1;
		P0 = polygons[0];
		P1 = polygons[1];
		UoP = SpatialUtilities.getUnion(P0, P1);
		for(int i=2;i<polygons.length;i++)
			UoP = SpatialUtilities.getUnion(UoP, polygons[i]);
		double unionArea = SpatialUtilities.getArea(UoP);
		return unionArea;
	}
	
	public static String getIntersection(String wktA, String wktB)
	{
		String intersection = "";
		try{
			
			WKTReader wktRdr = WKTReaderMaker.getWktReader();
			Geometry A = wktRdr.read(wktA);
			Geometry B = wktRdr.read(wktB);
			Geometry C = A.intersection(B);
			intersection = C.toString();
		} catch(Exception e){
			System.out.println("Error in getting intersection");
			System.err.println( e.getClass().getName()+": "+ e.getMessage() );
			return "GEOMETRYCOLLECTION EMPTY";
		}
		return intersection;
	}
	
	public static double getIntersectionArea(String[] polygons)
	{
		String IoP, P0, P1;
		P0 = polygons[0];
		P1 = polygons[1];
		IoP = SpatialUtilities.getIntersection(P0, P1);
		for(int i=2;i<polygons.length;i++)
			IoP = SpatialUtilities.getIntersection(IoP, polygons[i]);
		double intersectionArea = SpatialUtilities.getArea(IoP);
		return intersectionArea;
	}
	
	
}
