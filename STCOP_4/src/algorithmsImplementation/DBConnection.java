package algorithmsImplementation;

import java.sql.*;  
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/*
 * Written by: Shah Muhammad Hamdi
 * Date: 12/28/2015
 */

public class DBConnection {
	
	private static final String url_base = "jdbc:postgresql://";
	private static Connection general_connection = null;
	private static String url = null;
	private static Properties props = null;
	
	public static void establishConnnectionNoGUI()
	{
		Statement st;
		String sql;
		try {
			Class.forName("org.postgresql.Driver");
			//String url = "jdbc:postgresql://postgres:5432/"; //****dmlab server setting ****
			String url = "jdbc:postgresql://localhost:5432/";
			//String url = "jdbc:postgresql://dmlab.cs.gsu.edu:5432/";
			Properties props = new Properties();
			//props.setProperty("user","pgadimin");
			props.setProperty("user","postgres"); //****dmlab server setting ****
			//props.setProperty("password","r3mot3p8sswo4d"); //****dmlab server setting ****
			props.setProperty("password","");
			
			general_connection = DriverManager.getConnection(url, props);
			
			st = general_connection.createStatement();
			
			Statement stR;
			stR = general_connection.createStatement();		
			ResultSet rs;
			sql = "SELECT COUNT(1) FROM pg_database WHERE lower(datname) = lower('" + Constants.dbName + "');";
			rs = stR.executeQuery(sql);
			rs.next();
			int dbExists = rs.getInt(1);
			stR.close();
			rs.close();
			
			if (dbExists == 1) 
			{
				Constants.dbExists = true;
				url = url + Constants.dbName;
				general_connection = DriverManager.getConnection(url, props);
				System.out.println("Connection established successfully with "+Constants.dbName);		
			} 
			else 
			{
				Constants.dbExists = false;
				sql = "DROP DATABASE IF EXISTS "+Constants.dbName+";";
				st.execute(sql);
				sql = "CREATE DATABASE "+ Constants.dbName + ";";
				st.executeUpdate(sql);
				url = url + Constants.dbName;
				st.close();
				
				general_connection = DriverManager.getConnection(url, props);
				Statement st2;
				st2 = general_connection.createStatement();
				
				sql = "DROP EXTENSION IF EXISTS postgis CASCADE;";
				st2.execute(sql);
				sql = "DROP TABLE IF EXISTS spatial_ref_sys;";
				st2.execute(sql);
				sql = "CREATE EXTENSION postgis;";
				st2.executeUpdate(sql);
				
				st2.close();
				System.out.println("Database"+ Constants.dbName + "with extension created and connected successfully");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
	         System.out.println("Error in opening database by DBConnection.establishConnection");
	         System.err.println(e.getClass().getName()+": "+e.getMessage());
	         System.exit(0);
		}
	}
	
	public static void establishConnnection()
	{
		props = showDbConnectionPane();
		try {
	         Class.forName("org.postgresql.Driver");
	         System.out.println(url);
	         System.out.println(props);
	         general_connection = DriverManager.getConnection(url, props);
	         //int c = 1/0;
	        
	      } catch (Exception e) {
	         e.printStackTrace();
	         System.out.println("Error in opening database by DBConnection.establishConnection");
	         System.err.println(e.getClass().getName()+": "+e.getMessage());
	         System.exit(0);
	      }
	      System.out.println("Database connection established successfully by DBConnection.establishConnection !");
	}
	/*
		 * creates a JOptionPane and asks for connection parameters
		 * it sets a property object, then sets the url for postgresql database	
		 * 
		 * @return properties object for db connection
	*/
		private static Properties showDbConnectionPane() {
			Properties props = new Properties();
			JTextField userName = new JTextField();
			userName.setText("postgres");
			JTextField dbUrl= new JTextField();
			dbUrl.setText("localhost");
			JTextField dbPort = new JTextField();
			dbPort.setText("5432");
			JPasswordField password = new JPasswordField();
			password.setText("1");
			JTextField dbName = new JTextField();
			//dbName.setText("SpatioTemporalEventsDB"); 
			//dbName.setText("6K15");
			if (Constants.FILE_PATH.compareTo("F:\\STCOP_data_directory\\test arti\\")==0) {
				dbName.setText("testArti");
			}
			else if (Constants.FILE_PATH.compareTo("F:\\STCOP_data_directory\\data\\1Mo\\")==0) {
				dbName.setText("realData1Mon");
			}
			else if (Constants.FILE_PATH.compareTo("F:\\STCOP_data_directory\\data\\3Mo\\")==0) {
				dbName.setText("realDataQuarter");
			}
			else if (Constants.FILE_PATH.compareTo("F:\\STCOP_data_directory\\data\\6Mo\\")==0) {
				dbName.setText("realDataHalf");
			}
			else if (Constants.FILE_PATH.compareTo("F:\\STCOP_data_directory\\data\\12Mo\\")==0) {
				dbName.setText("realData1Year");
			}
			else if(Constants.FILE_PATH.compareTo("F:\\STCOP_data_directory\\6K_instances\\")==0){
				dbName.setText("6K15");
			}
			else if (Constants.FILE_PATH.compareTo("F:\\STCOP_data_directory\\test features\\")==0) {
				dbName.setText("smallReal");
			}
			else if (Constants.FILE_PATH.compareTo("F:\\STCOP_data_directory\\output1417900034638\\")==0) {
				dbName.setText("10Features");
			}
			else if (Constants.FILE_PATH.compareTo("F:\\STCOP_data_directory\\6K2\\")==0) {
				dbName.setText("6K2");
			}
		    Object[] message = {"User Name:", userName, 
		    					"DbUrl:", dbUrl, 
		    					"DbPort:", dbPort, 
		    					"DbName:", dbName, 
		    					"Password:", password};//send text of filename
		    
		    int option = JOptionPane.showConfirmDialog(null, message, "DB Connection", JOptionPane.OK_CANCEL_OPTION);
		    
		    if(option == JOptionPane.OK_OPTION){
		    	props.setProperty("user", userName.getText());
				props.setProperty("password",new String(password.getPassword()) );
					
				url = url_base + dbUrl.getText() + ":" + dbPort.getText() + "/" + dbName.getText() +"";
				System.out.println(url);
				System.out.println(props);
		    }
		    return props;
	}
	public static Connection getConnection()
	{
		try {
		do{
		if(!general_connection.isClosed()){
				return general_connection;
				} else {
//				establishConnnection();
				establishConnnectionNoGUI();
				return general_connection;
				}
			
		} while(general_connection.isClosed());
		} catch (SQLException e) {
		//e.printStackTrace();
		System.err.println(e);
		}		
		return null;
	}
	//public static 
	public static void closeConnection()
	{
		try{
		general_connection.close();
		} catch(Exception e){
		e.printStackTrace();
		System.out.println("Error in closeConnection().");
	    System.err.println(e.getClass().getName()+": "+e.getMessage()); 
	    System.exit(0);
		}
	}
	

}
