package algorithmsImplementation;

import java.sql.*;
import java.util.Set;
import java.util.TreeSet;
import java.io.*;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;

import com.vividsolutions.jts.io.WKTReader;

/*
 * Written by:	Shah Muhammad Hamdi
 * Date:	12/20/2015
 */
public class LoadDataInTablesFromFiles {

	public static void initializeData() {
		 Connection con = null;
	     Statement stmt = null;

		  try { 
		    /* 
		    * Load the JDBC driver and establish a connection. 
		    */
		    con = DBConnection.getConnection();
		    
		    stmt = con.createStatement();
		   
		    System.out.println("Inside load data module");
		    //Show all feature type names
		    System.out.println("All feature type names...");
		    for(int i=0;i<Constants.numberOfEventTypes;i++)
		    	System.out.println(Constants.eventTypes[i]);
		    
		    int table_name = 97; // Table names are 'a', 'b', 'c' and so on. 
		
		    String sql;
		    //final double BUFFER_SIZE = 0.0;
		    
		    for(int i=0;i<Constants.numberOfEventTypes;i++)
		    {
		    Set<Long> distinctInstances = new TreeSet<Long>();
		    sql = "DROP TABLE IF EXISTS \"" + ((char)(table_name + i)) + "\";";
		    stmt.execute(sql);		    
		    //Create all tables and insert data in them from data files
		    sql = "CREATE TABLE \"" + ((char)(table_name + i)) + "\" (instance_id BIGINT, "
		    		+ "generation_time BIGINT, "
		    		+ "PRIMARY KEY (instance_id, generation_time));"; 
            stmt.execute(sql);
            sql = "SELECT AddGeometryColumn('" + ((char)(table_name + i)) + "', "
            		+ "'polygon_coordinates', 4326, 'Polygon', 2);";
            stmt.execute(sql);
            //Reading rows from file and inserting into table
            BufferedReader br = new BufferedReader(new FileReader(Constants.files[i]));
            String line;
            while((line = br.readLine()) != null)
            {
            	String[] vals = line.split("\\t");
            	Long instanceId = Long.parseLong(vals[0]);
            	//make instanceID unique ; a --> i, b --> instanceID
				//Cantor pairing function
				instanceId = (((i+instanceId)*(i+instanceId+1))/2)+instanceId;
				distinctInstances.add(instanceId); //for reading number of distinct instances
        		Long generationTime = Long.parseLong(vals[1]);

        		try{
        		WKTReader wktRdr = WKTReaderMaker.getWktReader();
            	Geometry polygon;
            	if(vals.length==3) //Artificial Data
            	{	
            		polygon = wktRdr.read(vals[2]);  
            	}
            	else //Real data
            	{
            		polygon = wktRdr.read(Utilities.modify(vals[3]));
            		
            		if(!polygon.isValid())
            		{
            			polygon = polygon.convexHull();
            			polygon = DouglasPeuckerSimplifier.simplify(polygon, 1);
            		}
            		polygon = DouglasPeuckerSimplifier.simplify(polygon, 1);
                    polygon = polygon.convexHull();
            	}
            	sql = "INSERT INTO \""+((char)(table_name + i))
            			+ "\" (instance_id, generation_time, polygon_coordinates) "
            			+"VALUES("+ instanceId+", "+generationTime+", "+"ST_GeomFromText('"+polygon+"', 4326));";
            	stmt.execute(sql);
        		} catch(Exception e)
        		{
        			//System.out.println("Geometries can not be read.");
        			//System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        	        //System.exit(0);
        		}
            	
            }
            Constants.no_of_instances[i] = distinctInstances.size();
            br.close();
		    }
            
            stmt.close();
	  } catch ( Exception e ) {
        System.err.println( e.getClass().getName()+": "+ e.getMessage() );
      }
      System.out.println(Constants.numberOfEventTypes + " tables created successfully");
	}
}
