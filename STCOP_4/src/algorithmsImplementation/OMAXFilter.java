package algorithmsImplementation;

import java.sql.*;  
import java.util.*;
import java.lang.Long;


/*
 * Written by: Shah Muhammad Hamdi
 * Date: 12/24/2015
 */
public class OMAXFilter {
	public static double getOMAX(Long[] insid, Array ins_arr, String tablename)
	{
		double omax = 0.0;
		Connection con = null;
		String sql;
		try{
			con = DBConnection.getConnection();
			
			//Getting intersection area
			sql = "SELECT ST_AsText(intersection_geom) FROM \""+tablename+"\" WHERE ins_arr = '"+ins_arr+"';";
			ResultSet rs2 = null;
			Statement stmt2 = null;
			stmt2 = con.createStatement();
			rs2 = stmt2.executeQuery(sql);
			double intersectionArea = 0.0;
			while(rs2.next())
			{
				String currentMultiPolygon = rs2.getString(1);
				intersectionArea += SpatialUtilities.getArea(currentMultiPolygon);
			}
			stmt2.close();
			rs2.close();
			
			//Getting intersection volume
			double intersectionVolume = intersectionArea * (double)Constants.getDeltaT();
			
		//Getting maximum individual volume of the instances
		double[] individualVolume = new double[insid.length];
		for(int i=0;i<insid.length;i++)
		{
			double individualArea = 0.0;
			Statement st;
			ResultSet r;
			st = con.createStatement();
			sql = "SELECT ST_AsText(polygon_coordinates) FROM \""+tablename.charAt(i)+"\" WHERE instance_id = "+insid[i]+";";
			r = st.executeQuery(sql);
			while(r.next())
			{
				String currentPolygon = r.getString(1);
				individualArea += SpatialUtilities.getArea(currentPolygon);
			}
			individualVolume[i] = individualArea * (double)Constants.getDeltaT();
			st.close();
			r.close();
		}
		
		double maxIndividualVolume = Utilities.getMax(individualVolume);
		
		omax = intersectionVolume/maxIndividualVolume;
		
		}catch(Exception e){
		System.out.println("Error in getting OMAX of two trajectories");
		System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		return 0.0;
		}
		return omax;
	}
	
	public static void deleteOmaxFailedRows(List<String> c, int k)
	{
		double OMAX;
		Connection con;
	    Statement stmt;
	    String sql;    
	    /*Connecting to the database*/
	    try{
		con = DBConnection.getConnection();
		stmt =con.createStatement();
	  
		//Get OMAX value for each tuple of each table instance
	    //Delete OMAX failed rows
	    for(int i=0;i<c.size();i++)
		{
	    	String tablename = c.get(i);
		    Statement stmt1 = null ;
			sql = "SELECT DISTINCT ins_arr FROM \""+tablename+"\";";
			System.out.println(sql);
			stmt1 =con.createStatement();
			ResultSet rs = stmt1.executeQuery(sql) ;
			while(rs.next())
			{
				Array ins_arr = rs.getArray("ins_arr");
				Long[] insid = (Long[])ins_arr.getArray(); 
				System.out.println("Calling OMAX on: "+ins_arr);
				
				OMAX = OMAXFilter.getOMAX(insid, ins_arr, tablename);
				System.out.println("OMAX = "+ OMAX);
				
				if(OMAX < Constants.CCE_TH)
				{
					sql = "DELETE FROM \""+tablename+"\" WHERE ins_arr = '"+ins_arr+"';";
					stmt.executeUpdate(sql);
					System.out.println(sql);
				}
			}		
			stmt1.close();
			rs.close();
		}
		stmt.close();		
	    } catch(Exception e){
	    	System.out.println("Error in select or update query");
		    System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		    System.exit(0);
		}
	 }
}
