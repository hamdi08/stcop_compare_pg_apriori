package algorithmsImplementation;


public class PatternGrowthSTCOP {
	
	public static void PGCaller()
	{
		System.out.println("PG STCOP begins for "+Constants.FILE_PATH);
		Constants.callToJ = 0;
		Constants.numberOfPatternsForCCETest = 0;
		SetTransactions.makeTransactionFile();
		FPGrowthCaller.FPGrowthonTransactionFile();
		FPGrowthCaller.cleanProtoPatterns();
		PatternAnalyzer.refineProtoPatterns();
		PatternAnalyzer.refinePatterns();
		System.out.println("PG STCOP ends for "+Constants.FILE_PATH);
		Utilities.clearGlobalVariables();
	}

	public static void main(String[] args) {
		try{
			System.out.println("PG STCOP begins...");
			//Get connection with the database
			DBConnection.establishConnnection();
			Constants.callToJ = 0;
			Constants.numberOfPatternsForCCETest = 0;
			long startTime = System.currentTimeMillis();
			
			//Initializing WKTReader object
			WKTReaderMaker.initializeWKTReader();
			Constants.initializeConsantsParameters();
			
			/////////////////////////////LOAD DATA///////////////////////////
			long startTime_dataLoad = System.currentTimeMillis();
			//LoadDataInTablesFromFiles.initializeData();
			long endTime_dataLoad = System.currentTimeMillis();
			long time_dataload = endTime_dataLoad - startTime_dataLoad;
			Constants.setNumberOfInstances();
			System.out.println("Data load time: "+(double)time_dataload/1000+" seconds");
			
			/////////////////////////////GENERATE TABLE INSTANCES THROUGH APRIORI///////////// 
			long startTime_tabIns = System.currentTimeMillis();
			//possiblePatterns = SetTransactions.generateTableInstances();			
			long endTime_tabIns = System.currentTimeMillis();
			long time_tabIns = endTime_tabIns - startTime_tabIns;
			System.out.println("Table instance generation time: "+(double)time_tabIns/1000 + " seconds");
			
			
			//////////////////////////////MERGING TABLES////////////////////////////////
			long startTime_unionTables = System.currentTimeMillis();
			//SetTransactions.unionAllTableInstances(possiblePatterns);
			long endTime_unionTables = System.currentTimeMillis();
			long time_unionTables = endTime_unionTables - startTime_unionTables;
			System.out.println("Union tables time: "+(double)time_unionTables/1000 + " seconds");
			
			////////////////////////////DELETE DUPLICATES////////////////////////////////
			long startTime_deleteDuplicates = System.currentTimeMillis();
			//SetTransactions.deleteDuplicateTransactions();
			long endTime_deleteDuplicates = System.currentTimeMillis();
			long time_deleteDuplicates = endTime_deleteDuplicates - startTime_deleteDuplicates;
			System.out.println("Delete duplicates time: "+ (double)time_deleteDuplicates/1000 + " seconds");
			
			///////////////////////////MAKE TRANSACTION FILE////////////////////////////
			long startTime_makeFile = System.currentTimeMillis();
			SetTransactions.makeTransactionFile();
			long endTime_makeFile = System.currentTimeMillis();
			long time_makeFile = endTime_makeFile - startTime_makeFile;
			System.out.println("Make file time: "+(double) time_makeFile/1000 +" seconds");
			
			
			/////////////////////////FP GROWTH//////////////////////////////////////////
			long startTime_FP = System.currentTimeMillis();
			FPGrowthCaller.FPGrowthonTransactionFile();
			FPGrowthCaller.cleanProtoPatterns();
			long endTime_FP = System.currentTimeMillis();
			long time_FP = endTime_FP - startTime_FP;
			System.out.println("FP Growth time: "+ (double)time_FP/1000 + " seconds");
			
			
			
			/////////////////////REFINE PROTO PATTERNS (WITHOUT CCE)//////////////////
			long startTime_refineProtoPatterns = System.currentTimeMillis();
			PatternAnalyzer.refineProtoPatterns();
			long endTime_refineProtoPatterns = System.currentTimeMillis();
			long totalTime_refineProtoPatterns = endTime_refineProtoPatterns - startTime_refineProtoPatterns;
			System.out.println("Approximate PI refining time: "+ (double)totalTime_refineProtoPatterns/1000+" seconds");
			//output: refined_proto_patterns_with_PI.txt

			
			/////////////////////REFINE EXISTING PROTO PATTERNS (WITH CCE)//////////////////
			long startTime_refinePatterns = System.currentTimeMillis();
			PatternAnalyzer.refinePatterns();
			long endTime_refinePatterns = System.currentTimeMillis();
			long totalTime_refinePatterns = endTime_refinePatterns - startTime_refinePatterns;
			System.out.println("CCE and actual PI refining time: "+ (double) totalTime_refinePatterns/1000 + " seconds");
			//output: final_pattern_ids_with_pi.txt
			
			
			
			//close DB connection
			DBConnection.closeConnection();
			long endTime = System.currentTimeMillis();
			
			long totalTime = endTime - startTime;
			
			
			System.out.println("Calls to Jaccard: "+Constants.callToJ);
			System.out.println("Program run time: " + (double)totalTime/1000+ " seconds");
			
						
			
		}catch(Exception e){
			System.out.println("Error in controller module");
			e.printStackTrace();
		}
		System.out.println("PG STCOP terminates successfully"); 

	}

}
