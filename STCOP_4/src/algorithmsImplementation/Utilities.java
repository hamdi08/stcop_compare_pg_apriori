package algorithmsImplementation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.sql.*;
/*
 * Written by: Shah Muhammad Hamdi
 * Date: 12/21/2015
 */
public class Utilities {

public static void combine(String instr, StringBuffer outstr, List<String> c, int index, int k)
{
	//System.out.println(c);
	    for (int i = index; i < instr.length(); i++)
	    {
	        outstr.append(instr.charAt(i));
	        //System.out.println("outList: "+ outstr.toString());
	        if(outstr.toString().length()==k){
	        	c.add(outstr.toString());
	        	//System.out.println(c);
	        }
	        combine(instr, outstr, c, i + 1, k);
	        outstr.deleteCharAt(outstr.length() - 1);
	    }
} 
public static ArrayList<ArrayList<Integer>> powerSet(ArrayList<Integer> originalSet) {
	ArrayList<ArrayList<Integer>> sets = new ArrayList<ArrayList<Integer>>();
    if (originalSet.isEmpty()) {
        sets.add(new ArrayList<Integer>());
        return sets;
    }
    List<Integer> list = new ArrayList<Integer>(originalSet);
    Integer head = list.get(0);
    ArrayList<Integer> rest = new ArrayList<Integer>(list.subList(1, list.size()));
    for (ArrayList<Integer> set : powerSet(rest)) {
    	ArrayList<Integer> newSet = new ArrayList<Integer>();
        newSet.add(head);
        newSet.addAll(set);
        sets.add(newSet);
        sets.add(set);
    }
    return sets;
}

public static List<String> selfjoin(List<String> x, List<String> y, List<String> final_pattern_base, int size)
{
	List<String> result = new ArrayList<>();
	int i, j, k, l;
	boolean flag;
	
	l=0;
	
	for(i=0;i<x.size();i++)
		for(j=0;j<y.size();j++)
		{
			flag = true;
			for(k=0;k<size-1;k++)
			{
				if(x.get(i).charAt(k) == y.get(j).charAt(k))
					flag = true;
				else
				{
					flag = false;
					break;
				}
			}
			if(flag == true && x.get(i).charAt(size-1) < y.get(j).charAt(size-1))
			{
				result.add(l, x.get(i)+y.get(j).charAt(size-1));
				l = l + 1;
			}
		}
	if(size>1)
	{
	//System.out.println("final_pattern_base:"+final_pattern_base);
	//System.out.println("result:"+result);
	List<String> resultTemp = new ArrayList<String>();
	resultTemp.addAll(result);
	
	for(i=0;i<result.size();i++)
	{
		List<String> ss = new ArrayList<String>();
		combine(resultTemp.get(resultTemp.indexOf(result.get(i))), new StringBuffer(), ss,  0, size);
		//System.out.println("ss: "+ss);
		//All current candidates must remain in the list of previous final_pattern_base
		for(j=0;j<ss.size();j++)
			if(!final_pattern_base.contains(ss.get(j)))
			{
				resultTemp.remove(result.get(i));
				break;
			}
	}
	result = resultTemp;
	}
	return result;
}

public static ArrayList<ArrayList<Integer>> selfjoinWithLists(ArrayList<ArrayList<Integer>> x, ArrayList<ArrayList<Integer>> y, int individualListSize, ArrayList<ArrayList<Integer>> allFrequentPatterns)
{
	ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
	int i, j, k;
	boolean flag;
	
	//System.out.println("inside self join function");
	//System.out.println(x);
	for(i=0;i<x.size();i++)
		for(j=0;j<y.size();j++)
		{
			//System.out.println(x.get(i));
			flag = true;
			for(k=0;k<individualListSize-1;k++)
			{
				if((int)x.get(i).get(k)==(int)y.get(j).get(k))
					flag = true;
				else
				{
					flag = false;
					break;
				}
			}
			if(flag == true && (int)x.get(i).get(individualListSize-1) < (int)y.get(j).get(individualListSize-1))
			{
				ArrayList<Integer> list1 = new ArrayList<Integer>();
				list1.addAll(x.get(i));
				//System.out.println("list1: "+list1);
				Integer lastElementofList2 = y.get(j).get(individualListSize-1);
				//System.out.println("addeee:"+lastElementofList2);
				list1.add(lastElementofList2);
				//System.out.println("x.get(i)"+x.get(i));
				result.add(list1);
				//list1.to
			}
		}
	if(individualListSize > 1)
	{
		//System.out.println("all Frequent Patterns: "+ allFrequentPatterns);
		//System.out.println("result:"+result);
		ArrayList<ArrayList<Integer>> resultTemp = new ArrayList<ArrayList<Integer>>();
		resultTemp.addAll(result);
		Iterator<ArrayList<Integer>> itResult = result.iterator();
		while(itResult.hasNext())
		{
			ArrayList<Integer> resultVal = itResult.next();
			ArrayList<Integer> resultTempVal = resultTemp.get(resultTemp.indexOf(resultVal));
			ArrayList<ArrayList<Integer>> ss = new ArrayList<ArrayList<Integer>>();
			ss = powerSet(resultTempVal);
			//System.out.println("ss: "+ss);
			Iterator<ArrayList<Integer>> itSS = ss.iterator();
			while(itSS.hasNext())
			{
				ArrayList<Integer> ssElement = itSS.next();
				//System.out.println(ssElement);
				if(ssElement.size() == individualListSize)
				{
					if(!allFrequentPatterns.contains(ssElement))
					{
						//System.out.println("Deleted: "+resultVal);
						resultTemp.remove(resultVal);
						break;
					}
				}
				else 
					continue;
			}
		}
		System.out.println("resultTemp: "+resultTemp);
		result = resultTemp;
		
	}
	return result;
}


public static double getMax(double x[])
{
	double max = x[0];
	for(int i=1;i<x.length;i++)
		if(x[i] > max)
			max = x[i];
	return max;
}

public static double getMin(double x[])
{
	double min = x[0];
	for(int i=1;i<x.length;i++)
		if(x[i] < min)
			min = x[i];
	return min;
}

public static Long getMaxLong(Long x[])
{
	Long max = x[0];
	for(int i=1;i<x.length;i++)
		if(x[i] > max)
			max = x[i];
	return max;
}

public static Long getMinLong(Long x[])
{
	Long min = x[0];
	for(int i=1;i<x.length;i++)
		if(x[i] < min)
			min = x[i];
	return min;
}


public static void showOrginalPatterns(List<String> p)
{
	for(int i=0;i<p.size();i++)
	{
		System.out.print("{ ");
		for(int j=0;j<p.get(i).length();j++)
		{
			char ch = p.get(i).charAt(j);
			int in = (int)ch - 97 ;
			System.out.print(Constants.eventTypes[in] + ", ");
		}
		System.out.println("} ");
	}
		
}
public static String[] getTwoSmallTables(HashMap<String,Integer> hm2, String tablename, int k)
{
	//sorting hm2
	Object[] a = hm2.entrySet().toArray();
	Arrays.sort(a, new Comparator() {
        public int compare(Object o1, Object o2) {
            return ((Map.Entry<String, Integer>) o1).getValue().compareTo(
                    ((Map.Entry<String, Integer>) o2).getValue());
        }
    });
	
	List<String> substrs = new ArrayList<String>();
	Utilities.combine(tablename, new StringBuffer(), substrs, 0, k);
	String[] smallTables = new String[2];
	int l = 0;
	
	//System.out.println("After sorting...");
	for (Object e : a) {
        //System.out.println(((Map.Entry<String, Integer>) e).getKey() + " : "
        //        + ((Map.Entry<String, Integer>) e).getValue());
		if(substrs.contains(((Map.Entry<String, Integer>) e).getKey()))
		{
			smallTables[l] = ((Map.Entry<String, Integer>) e).getKey();
			l = l + 1;
		}
		if(l == 2)
			break;
    }
	
	return smallTables;
}
public static String modify(String s) {
    int index1 = s.lastIndexOf("(");
    int index2 = s.indexOf(")");
    String pointStr = s.substring(index1 + 1, index2);
    String[] points = pointStr.split(",");
    return s.substring(0, index2) + "," + points[0] + "))";
}

public static Long[] merge(String tablename, String t1, Long[] a1, String t2, Long[] a2)
{
	Long[] arr = new Long[tablename.length()];
	for(int i=0;i<tablename.length();i++)
	{
		String x = Character.toString(tablename.charAt(i));
		if(t1.contains(x))
				arr[i] = a1[t1.indexOf(x)];
		else
				arr[i] = a2[t2.indexOf(x)];			
	}
	return arr;
}

public static String getFormattedArray(Long[] arr)
{
	String x = "{";
	for(int i=0;i<arr.length;i++)
		x += Long.toString(arr[i])+", ";
	x = x.substring(0, x.length()-2) + "}";
	return x;
}

public static void showPatterns()
{
	try{
		BufferedReader br = new BufferedReader(new FileReader("final_pattern_ids_with_pi.txt"));
		Formatter output = new Formatter("final_patterns.txt");
		String line;
		//double originalPI;
		while((line=br.readLine())!=null)
		{
			String[] pat_pi = line.split(":");
			String pattern = pat_pi[0]; //of the form [0, 1]
			pattern = pattern.trim().replace("[", "");
			pattern = pattern.trim().replace(",", "");
			pattern = pattern.trim().replace("]", "");
			String[] feature = pattern.split(" ");
			int[] featureID = new int[feature.length];
			for(int i=0;i<feature.length;i++)
			{
				featureID[i] = Integer.parseInt(feature[i]);
				output.format("%s, ", Constants.eventTypes[featureID[i]]);
				//System.out.println("ok");
			}
			output.format("\n");
		}
		br.close();
		output.close();
	}catch(Exception e)
	{
		System.out.println("Error in showPatterns");
		System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		System.exit(0);
	}
}

public static Set<Set<String>> cartesianProduct(List<Set<String>> sets) {
	//System.out.println("Alive0");
    if (sets.size() < 2)
        throw new IllegalArgumentException(
                "Can't have a product of fewer than two sets (got " +
                sets.size() + ")");

    return _cartesianProduct(0, sets);
}

private static Set<Set<String>> _cartesianProduct(int index, List<Set<String>> sets) {
	//System.out.println("Alive1");
    Set<Set<String>> ret = new HashSet<Set<String>>();
    //System.out.println("Alive2");
    if (index == sets.size()) {
        ret.add(new HashSet<String>());
    } 
    else 
    {
        for (String obj : sets.get(index)) {
            for (Set<String> set : _cartesianProduct(index+1, sets)) {
                set.add(obj);
                ret.add(set);
            }
        }
    }
    return ret;
}

public static void setSpatialIntersectionExceptionHandler()
{
	Connection con = null;
	String sql;
	try {
		con = DBConnection.getConnection();
		Statement st = con.createStatement();
		sql = "DROP FUNCTION IF EXISTS intersection_with_exception_handler(GEOMETRY, GEOMETRY);";
		st.execute(sql);
		sql = "CREATE OR REPLACE FUNCTION intersection_with_exception_handler(g1 GEOMETRY, g2 GEOMETRY) RETURNS GEOMETRY AS "
				+ "$BODY$ "
				+ "DECLARE "
				+ "intersection_geom GEOMETRY; "
				+ "BEGIN "
				+ "intersection_geom = ST_Multi(ST_Intersection(g1, g2)); "
				+ "RETURN intersection_geom; "
				+ "EXCEPTION "
				+ "WHEN OTHERS THEN RETURN NULL;   "
				+ "END; "
				+ "$BODY$ "
				+ "LANGUAGE 'plpgsql';";
		st.execute(sql);
		st.close();
	} catch (Exception e) {
		System.out.println("Error in setSpatialIntersectionExceptionHandler");
		e.printStackTrace();
		System.exit(0);
	}
}

public static void clearGlobalVariables()
{
	PatternAnalyzer.JaccardTrash.clear();
	PatternAnalyzer.allTableInstances.clear();
}
}
